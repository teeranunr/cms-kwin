﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="import_game.aspx.vb" Inherits="CMS_Kwin.import_game" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <br>
    <div class="row">
        <div class="col-md-4" style="text-align:right">
            SELECT SERVICE : 
        </div>
        <div class="col-md-8" style="text-align:left">
            <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True">
            </asp:DropDownList>
        </div>  
    </div>
    <div class="row" style="height:10px;">
    </div> 
    <div class="row">
        <div class="col-md-4" style="text-align:right">
            <asp:Label ID="lblSelectFile" runat="server" Text="SELECT FILE. :" Visible="false" ></asp:Label>             
        </div>
        <div class="col-md-8" style="text-align:left">
            <asp:FileUpload ID="FileUpload1" runat="server" Visible="false"  /> 
        </div>  
    </div>
    <div class="row" style="height:10px;">
    </div>  
    <div class="row">
        <div class="col-md-4">
            
        </div>
        <div class="col-md-8" style="text-align:left">
            <asp:Label ID="lblRemark" runat="server" Text="File format .xls,.xlsx" Visible="false"></asp:Label>
        </div>  
    </div>
    <div class="row" style="height:10px;">
    </div>  
    <div class="row">
        <div class="col-md-4">
            
        </div>
        <div class="col-md-8" style="text-align:left">
            <asp:Button ID="Button1" runat="server" Text="IMPORT" Visible="false" />
        </div>  
    </div>
    <hr />
    <div class="row">
        <div class="col-md-4">
            
        </div>
        <div class="col-md-8" style="text-align:left">
            <asp:Label ID="lblError" runat="server" Text="" Visible="false" ForeColor="Red"></asp:Label>
        </div> 
    </div> 
    <div class="row">
        <div class="col-md-12">
            <asp:GridView ID="GridView1" runat="server" BackColor="White" BorderColor="#999999" 
                BorderStyle="Solid" BorderWidth="1px" CellPadding="3" ForeColor="Black" 
                GridLines="Vertical" AutoGenerateColumns="False" DataKeyNames="CNT_ID" 
                OnRowDeleting="GridView1_RowDeleting" OnRowDataBound="GridView1_RowDataBound" 
                PageSize="20" AllowPaging="True" HeaderStyle-Font-Size="10px"
                OnPageIndexChanging="GridView1_PageIndexChanging" Width="100%">
                <AlternatingRowStyle BackColor="#CCCCCC"/>
                <Columns>
                    <%--<asp:BoundField HeaderText="CNT_ID" DataField="CNT_ID" />--%>
<%--                    <asp:BoundField HeaderText="SERVICE" DataField="SV_NAME" HeaderStyle-HorizontalAlign="Center">
                        <ItemStyle Width="10%"></ItemStyle>
                    </asp:BoundField>--%>
                    <asp:BoundField HeaderText="QUESTION" DataField="CNT_DESC">
                        <ItemStyle Width="20%"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField HeaderText="CHOICE" DataField="CNT_CHOICE">
                        <ItemStyle Width="20%"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField HeaderText="ANSWER" DataField="CNT_RESULT">
                        <ItemStyle Width="20%"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField HeaderText="CHOICE ANSWER" DataField="CNT_ANSWER">
                        <ItemStyle Width="5%" HorizontalAlign="Center"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField HeaderText="SCHEDULE DATE" DataField="SCHEDULE_DATE">
                        <ItemStyle Width="15%" HorizontalAlign="Center"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField HeaderText="CREATE DATE" DataField="CREATE_DATE">
                        <ItemStyle Width="15%" HorizontalAlign="Center"></ItemStyle>
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="STATUS">
                        <itemtemplate>
							<%# GetStatus(Container.DataItem("STATUS"))%>
						</itemtemplate>
                    </asp:TemplateField>                    
                    <asp:CommandField ShowDeleteButton="True" ItemStyle-HorizontalAlign="Center">
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:CommandField>
                </Columns>
                <FooterStyle BackColor="#CCCCCC" />
                <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                <SortedAscendingHeaderStyle BackColor="#808080" />
                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                <SortedDescendingHeaderStyle BackColor="#383838" />
            </asp:GridView>
        </div>  
    </div> 
    
</asp:Content>
