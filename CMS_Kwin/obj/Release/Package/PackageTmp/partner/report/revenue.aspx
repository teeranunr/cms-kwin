﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="revenue.aspx.vb" Inherits="CMS_Kwin.revenue1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
      <div class="col-md-12" style="height:10px;" >

      </div>  
    </div> 
    <div class="row">
        <div class="col-xs-12 col-md-12 col-sm-12 text-center" style="margin-top:10px;">
            <asp:Label ID="Label2" runat="server" Text="START DATE: "></asp:Label>
            <asp:TextBox ID="txbStart" runat="server"></asp:TextBox>
            <asp:Button ID="btnStart" runat="server" Text="..." />
            <div id="divCalendar" style="POSITION: absolute; z-index:10; left: 30%; top: 50%; visibility: visible;">
                <asp:Calendar ID="cldStart" runat="server" BackColor="White"></asp:Calendar>                 
            </div>&nbsp;&nbsp; 
            <asp:Label ID="Label1" runat="server" Text="END DATE: "></asp:Label>
            <asp:TextBox ID="txbEnd" runat="server" ></asp:TextBox>
            <asp:Button ID="btnEnd" runat="server" Text="..." />
            <div id="div1" style="POSITION:absolute; z-index:10; left:50%; top:50%; visibility:visible;">
                <asp:Calendar ID="cldEnd" runat="server" BackColor="White"></asp:Calendar>  
            </div> 
        </div>
<%--        <div class="col-md-6" style="text-align:left">
          
        </div>--%>
    </div>
    <br />
    <div class="row">
        <div class="col-md-12" style="text-align:center;">
            <asp:Label ID="lblOperator" runat="server" Text="OPERATOR: " Visible="false" ></asp:Label><asp:DropDownList ID="ddlOperator" runat="server" Visible="false"></asp:DropDownList>
            &nbsp;&nbsp;<asp:Label ID="lblAppId" runat="server" Text="  APP ID: " Visible="false" ></asp:Label>
            <asp:DropDownList ID="ddlAppId" runat="server" Visible="false"></asp:DropDownList>
            &nbsp;&nbsp;<asp:Label ID="lblServicePartner" runat="server" Text="  SERVICE: " Visible="false" ></asp:Label>
            <asp:DropDownList ID="ddlServicePartner" runat="server" Visible="false"></asp:DropDownList>
        </div>
    </div>
            
        <div class="row" style="height:10px">
       
    </div>
    <div class="row">
        <div class="col-md-6" style="text-align:right">
            <asp:Label ID="lblService" runat="server" Text="SERVICE:"></asp:Label>
             <asp:DropDownList ID="ddlService" runat="server" Visible="false" >
             </asp:DropDownList> 
        </div> 
        <div class="col-md-6" style="text-align:left">
             <asp:Label ID="lblTime" runat="server" Text="  TIME: " Visible="false" ></asp:Label>
                <asp:DropDownList ID="ddlStartTime" runat="server" Visible="false">
                <asp:ListItem Text="00:00" Value="00:00"></asp:ListItem>
                <asp:ListItem Text="01:00" Value="01:00"></asp:ListItem>
                <asp:ListItem Text="02:00" Value="02:00"></asp:ListItem>
                <asp:ListItem Text="03:00" Value="03:00"></asp:ListItem>
                <asp:ListItem Text="04:00" Value="04:00"></asp:ListItem>
                <asp:ListItem Text="05:00" Value="05:00"></asp:ListItem>
                <asp:ListItem Text="06:00" Value="06:00"></asp:ListItem>
                <asp:ListItem Text="07:00" Value="07:00"></asp:ListItem>
                <asp:ListItem Text="08:00" Value="08:00"></asp:ListItem>
                <asp:ListItem Text="09:00" Value="09:00"></asp:ListItem>
                <asp:ListItem Text="10:00" Value="10:00"></asp:ListItem>
                <asp:ListItem Text="11:00" Value="11:00"></asp:ListItem>
                <asp:ListItem Text="12:00" Value="12:00"></asp:ListItem>
                <asp:ListItem Text="13:00" Value="13:00"></asp:ListItem>
                <asp:ListItem Text="14:00" Value="14:00"></asp:ListItem>
                <asp:ListItem Text="15:00" Value="15:00"></asp:ListItem>
                <asp:ListItem Text="16:00" Value="16:00"></asp:ListItem>
                <asp:ListItem Text="17:00" Value="17:00"></asp:ListItem>
                <asp:ListItem Text="18:00" Value="18:00"></asp:ListItem>
                <asp:ListItem Text="19:00" Value="19:00"></asp:ListItem>
                <asp:ListItem Text="20:00" Value="20:00"></asp:ListItem>
                <asp:ListItem Text="21:00" Value="21:00"></asp:ListItem>
                <asp:ListItem Text="22:00" Value="22:00"></asp:ListItem>
                <asp:ListItem Text="23:00" Value="23:00"></asp:ListItem>
                <asp:ListItem Text="24:00" Value="24:00"></asp:ListItem>
            </asp:DropDownList>  
            <asp:DropDownList ID="ddlStopTime" runat="server">
                <asp:ListItem Text="00:00" Value="00:00"></asp:ListItem>
                <asp:ListItem Text="01:00" Value="01:00"></asp:ListItem>
                <asp:ListItem Text="02:00" Value="02:00"></asp:ListItem>
                <asp:ListItem Text="03:00" Value="03:00"></asp:ListItem>
                <asp:ListItem Text="04:00" Value="04:00"></asp:ListItem>
                <asp:ListItem Text="05:00" Value="05:00"></asp:ListItem>
                <asp:ListItem Text="06:00" Value="06:00"></asp:ListItem>
                <asp:ListItem Text="07:00" Value="07:00"></asp:ListItem>
                <asp:ListItem Text="08:00" Value="08:00"></asp:ListItem>
                <asp:ListItem Text="09:00" Value="09:00"></asp:ListItem>
                <asp:ListItem Text="10:00" Value="10:00"></asp:ListItem>
                <asp:ListItem Text="11:00" Value="11:00"></asp:ListItem>
                <asp:ListItem Text="12:00" Value="12:00"></asp:ListItem>
                <asp:ListItem Text="13:00" Value="13:00"></asp:ListItem>
                <asp:ListItem Text="14:00" Value="14:00"></asp:ListItem>
                <asp:ListItem Text="15:00" Value="15:00"></asp:ListItem>
                <asp:ListItem Text="16:00" Value="16:00"></asp:ListItem>
                <asp:ListItem Text="17:00" Value="17:00"></asp:ListItem>
                <asp:ListItem Text="18:00" Value="18:00"></asp:ListItem>
                <asp:ListItem Text="19:00" Value="19:00"></asp:ListItem>
                <asp:ListItem Text="20:00" Value="20:00"></asp:ListItem>
                <asp:ListItem Text="21:00" Value="21:00"></asp:ListItem>
                <asp:ListItem Text="22:00" Value="22:00"></asp:ListItem>
                <asp:ListItem Text="23:00" Value="23:00"></asp:ListItem>
                <asp:ListItem Text="24:00" Value="24:00" Selected="True" ></asp:ListItem>
             </asp:DropDownList>
      </div> 
    </div>
    <div class="row" style="height:10px">
       
    </div>
    <div class="row">
        <div class="col-md-12" style="text-align:center">
            <asp:Button ID="btnSearch" runat="server" Text="SEARCH" CssClass="btn btn-default"/>
        </div> 
    </div>
    <div class="row" style="height:10px">
       
    </div>
    <div class="row">
        <div class="col-md-12" style="text-align:center">
            <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
        </div> 
    </div>
    <div class="row">
      <div class="col-md-12" style="height:10px;" >

      </div>  
    </div>
        <div class="row">
        <div class="col-md-12" style="text-align:right">
            <asp:Button ID="btnExportExcel" runat="server" Text="EXPORT" Visible="false" CssClass="btn btn-default"/>
        </div> 
    </div>
        <div class="row">
      <div class="col-md-12" style="height:10px;" >

      </div>  
    </div>
    <div class="row">
        <div class="col-md-12">
            <asp:GridView ID="GridView1" runat="server" Width="100%" AutoGenerateColumns="False" 
                HeaderStyle-BackColor="Black" HeaderStyle-ForeColor="White" Visible="false" 
                ShowFooter="true" FooterStyle-BackColor="Black" FooterStyle-ForeColor="White" OnDataBound="GridView1_DataBound"
                FooterStyle-HorizontalAlign="Right">
                <Columns>
                    <asp:BoundField DataField="YYYYMMDD" HeaderText="Date" SortExpression="YYYYMMDD" ItemStyle-Width="20%" ItemStyle-HorizontalAlign="Center">                    
                        <ItemStyle HorizontalAlign="Center" Width="15%"></ItemStyle>
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Service" ItemStyle-Width="35%" ItemStyle-HorizontalAlign="left">
                        <itemtemplate>
					        <%# GetServiceName(Container.DataItem("SV_ID"))%>
				        </itemtemplate>
                        <ItemStyle HorizontalAlign="Left" Width="25%"></ItemStyle>
                    </asp:TemplateField>
                    <asp:BoundField DataField="SUBS_NUM" HeaderText="Subscription" ItemStyle-Width="15%" 
                        ItemStyle-HorizontalAlign="right" DataFormatString="{0:###,##0}">
                        <ItemStyle HorizontalAlign="Right" Width="15%"></ItemStyle>
                    </asp:BoundField> 
                    <asp:BoundField DataField="UNSUBS_NUM" HeaderText="Cancel" ItemStyle-Width="15%" 
                        ItemStyle-HorizontalAlign="right" DataFormatString="{0:###,##0}">
                        <ItemStyle HorizontalAlign="Right" Width="15%"></ItemStyle>
                    </asp:BoundField> 
                    <asp:BoundField DataField="ACTIVE_NUM" HeaderText="Active" ItemStyle-Width="15%" 
                        ItemStyle-HorizontalAlign="right" DataFormatString="{0:###,##0}">
                        <ItemStyle HorizontalAlign="Right" Width="15%"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="TOTAL_ACTIVE" HeaderText="Total Active" ItemStyle-Width="15%" 
                        ItemStyle-HorizontalAlign="right" DataFormatString="{0:###,##0}">
                        <ItemStyle HorizontalAlign="center" Width="15%"></ItemStyle>
                    </asp:BoundField>                    
                </Columns>
            </asp:GridView>
            <asp:GridView ID="GridView2" runat="server" Width="100%" AutoGenerateColumns="False" 
                HeaderStyle-BackColor="Black" HeaderStyle-ForeColor="White" Visible="false" 
                ShowFooter="true" FooterStyle-BackColor="Black" FooterStyle-ForeColor="White"
                FooterStyle-HorizontalAlign="Right">
                <Columns>
                    <asp:BoundField DataField="YYYYMMDD" HeaderText="Date" SortExpression="YYYYMMDD" ItemStyle-HorizontalAlign="Center">                    
                        <ItemStyle HorizontalAlign="Center" Width="20%"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="TOTAL_TRANS" HeaderText="Transaction Gen" ItemStyle-HorizontalAlign="right" DataFormatString="{0:###,##0}">
                        <ItemStyle HorizontalAlign="Right" Width="10%"></ItemStyle>
                    </asp:BoundField> 
                    <asp:BoundField DataField="TOTAL_SENT_TRANS" HeaderText="Transaction Sent (Include Retry)" ItemStyle-HorizontalAlign="right" DataFormatString="{0:###,##0}">
                        <ItemStyle HorizontalAlign="Right" Width="10%"></ItemStyle>
                    </asp:BoundField> 
                    <asp:BoundField DataField="SUCCESS_TRANS" HeaderText="Success" ItemStyle-HorizontalAlign="right" DataFormatString="{0:###,##0}">
                        <ItemStyle HorizontalAlign="Right" Width="10%"></ItemStyle>
                    </asp:BoundField> 
                    <asp:BoundField DataField="TOTAL_AMOUNT" HeaderText="Charged Amount (Baht)" ItemStyle-HorizontalAlign="right" DataFormatString="{0:###,##0}">
                        <ItemStyle HorizontalAlign="Right" Width="10%"></ItemStyle>
                    </asp:BoundField> 
                    <asp:BoundField DataField="SUCCESS_RATE" HeaderText="Success Rate" ItemStyle-HorizontalAlign="right">
                        <ItemStyle HorizontalAlign="Right" Width="10%"></ItemStyle>
                    </asp:BoundField>                    
                </Columns>
            </asp:GridView>
            <asp:GridView ID="GridView3" runat="server" Width="100%" AutoGenerateColumns="False" 
                HeaderStyle-BackColor="Black" HeaderStyle-ForeColor="White" 
                Visible="False" DataKeyNames="SV_ID" OnDataBound="GridView31_DataBound"
                ShowFooter="true" FooterStyle-BackColor="Black" FooterStyle-ForeColor="White"
                FooterStyle-HorizontalAlign="Right">
                <Columns>
                    <asp:BoundField DataField="YYYYMMDD" HeaderText="Date" SortExpression="YYYYMMDD" ItemStyle-HorizontalAlign="Center">                    
                        <ItemStyle HorizontalAlign="Center" Width="12%"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="SV_NAME" HeaderText="Service" ItemStyle-HorizontalAlign="right">
                        <ItemStyle HorizontalAlign="Left" Width="10%"></ItemStyle>                         
                    </asp:BoundField> 
                    <asp:BoundField DataField="CNT_DESC" HeaderText="Content" ItemStyle-HorizontalAlign="right">
                        <ItemStyle HorizontalAlign="Left" Width="30%"></ItemStyle>                         
                    </asp:BoundField>  
                    <asp:BoundField DataField="TOTAL_TRANS" HeaderText="Transaction Gen"
                        ItemStyle-HorizontalAlign="right" DataFormatString="{0:###,##0}">
                        <ItemStyle HorizontalAlign="Right" Width="12%"></ItemStyle>
                    </asp:BoundField> 
                    <asp:BoundField DataField="TOTAL_SENT_TRANS" HeaderText="Total Sent Trans (Include Retry)" ItemStyle-HorizontalAlign="right" DataFormatString="{0:###,##0}">
                        <ItemStyle HorizontalAlign="Right" Width="12%"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="SUCCESS_TRANS" HeaderText="Success Trans" 
                        ItemStyle-HorizontalAlign="right" DataFormatString="{0:###,##0}">
                        <ItemStyle HorizontalAlign="Right" Width="12%"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="TOTAL_AMOUNT" HeaderText="Amount" 
                        ItemStyle-HorizontalAlign="right" DataFormatString="{0:###,##0}">
                        <ItemStyle HorizontalAlign="Right" Width="12%"></ItemStyle>
                    </asp:BoundField>                       
                </Columns>
                <HeaderStyle BackColor="Black" ForeColor="White"></HeaderStyle>
            </asp:GridView>
            <asp:GridView ID="GridView4" runat="server" Width="100%" AutoGenerateColumns="False" 
                HeaderStyle-BackColor="Black" HeaderStyle-ForeColor="White" Visible="false" 
                ShowFooter="true" FooterStyle-BackColor="Black" FooterStyle-ForeColor="White" FooterStyle-HorizontalAlign="Right">
                <Columns>
                    <asp:BoundField DataField="YYYYMMDD" HeaderText="Date" ItemStyle-HorizontalAlign="Center">                    
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="ROUND_NO" HeaderText="Round" ItemStyle-HorizontalAlign="Center">                    
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="A_SENT_TIME" HeaderText="Sent Time" ItemStyle-HorizontalAlign="Center">                    
                        <ItemStyle HorizontalAlign="center"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="A_SENT_TRANS" HeaderText="Sent-Transaction" ItemStyle-HorizontalAlign="Center"
                        DataFormatString="{0:###,##0}">                    
                        <ItemStyle HorizontalAlign="right" ></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="A_DR_TRANS" HeaderText="DR-Transaction" ItemStyle-HorizontalAlign="Center"
                        DataFormatString="{0:###,##0}">                    
                        <ItemStyle HorizontalAlign="right"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="A_SUCCESS_TRANS" HeaderText="DR-Success" ItemStyle-HorizontalAlign="Center"
                        DataFormatString="{0:###,##0}">                    
                        <ItemStyle HorizontalAlign="right"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="Q_SENT_TIME" HeaderText="Sent Time" ItemStyle-HorizontalAlign="Center">                    
                        <ItemStyle HorizontalAlign="center"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="Q_SENT_TRANS" HeaderText="Sent-Transaction" ItemStyle-HorizontalAlign="Center"
                        DataFormatString="{0:###,##0}">                    
                        <ItemStyle HorizontalAlign="right"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="Q_DR_TRANS" HeaderText="DR-Transaction" ItemStyle-HorizontalAlign="Center"
                        DataFormatString="{0:###,##0}">                    
                        <ItemStyle HorizontalAlign="right"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="Q_SUCCESS_TRANS" HeaderText="DR-Success" ItemStyle-HorizontalAlign="Center"
                        DataFormatString="{0:###,##0}">                    
                        <ItemStyle HorizontalAlign="right"></ItemStyle>
                    </asp:BoundField> 
                    <asp:BoundField DataField="C_SENT_TIME" HeaderText="Sent Time" ItemStyle-HorizontalAlign="Center">                    
                        <ItemStyle HorizontalAlign="center"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="C_SENT_TRANS" HeaderText="Sent-Transaction" ItemStyle-HorizontalAlign="Center"
                        DataFormatString="{0:###,##0}">                    
                        <ItemStyle HorizontalAlign="right"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="C_DR_TRANS" HeaderText="DR-Transaction" ItemStyle-HorizontalAlign="Center"
                        DataFormatString="{0:###,##0}">                    
                        <ItemStyle HorizontalAlign="right"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="C_SUCCESS_TRANS" HeaderText="DR-Success" ItemStyle-HorizontalAlign="Center"
                        DataFormatString="{0:###,##0}">                    
                        <ItemStyle HorizontalAlign="right"></ItemStyle>
                    </asp:BoundField>                                         
                </Columns>
            </asp:GridView>
            <div class="row" style="height:10px">

            </div>
           <div class="row" runat="server" id="div_diff" visible="false" style="font-weight:bold" >
                <div class="col-md-3" style="text-align:left" runat="server"> 
                    <asp:Label ID="Label3" runat="server" Text="A : เฉลย วันก่อนหน้า"></asp:Label>
                </div>
                <div class="col-md-3" style="text-align:left">
                    <asp:Label ID="Label4" runat="server" Text="Q : คำถาม ประจำวัน"></asp:Label>
                </div> 
                <div class="col-md-6" style="text-align:left">
                    <asp:Label ID="Label5" runat="server" Text="C : ตัวเลือก ของคำถามประจำวัน"></asp:Label>
                </div> 
            </div> 
            <div class="row" runat="server" id="div_diff2" visible="false" style="font-weight:bold">
                <div class="col-md-3" style="text-align:left" runat="server"> 
                    <asp:Label ID="lblDiffTransA" runat="server" Text=""></asp:Label>
                </div>
                <div class="col-md-3" style="text-align:left">
                    <asp:Label ID="lblDiffTransQ" runat="server" Text=""></asp:Label>
                </div> 
                <div class="col-md-6" style="text-align:left">
                    <asp:Label ID="lblDiffTransC" runat="server" Text=""></asp:Label>
                </div>  
            </div>
            <div class="row" runat="server" id="div_diff3" visible="false" style="font-weight:bold">
                <div class="col-md-3" style="text-align:left">
                     <asp:Label ID="lblChargedAmountA" runat="server" Text=""></asp:Label>
                </div>
                <div class="col-md-3" style="text-align:left">
                    <asp:Label ID="lblChargedAmountQ" runat="server" Text=""></asp:Label>
                </div> 
                <div class="col-md-6" style="text-align:left">
                    <asp:Label ID="lblChargedAmountC" runat="server" Text=""></asp:Label>
                </div>  
            </div>
            <div class="row" style="height:10px">
                <div class="col-md-12" style="text-align:left">
                
                </div> 
            </div> 
            <div class="row">
                <div class="col-md-12" style="text-align:left; font-weight:bold">
                    <asp:Label ID="lblSummary" runat="server" Text="Summary" Visible="false" ></asp:Label>        
                </div> 
            </div> 
            <div class="row">
                <div class="col-md-12" style="text-align:left; font-weight:bold">
                    <asp:Label ID="lblSentTransaction" runat="server" Text="" Visible="false" ></asp:Label>
                </div> 
            </div>
            <div class="row">
                <div class="col-md-12" style="text-align:left; font-weight:bold">
                    <asp:Label ID="lblDRTransaction" runat="server" Text="" Visible="false" ></asp:Label>
                </div> 
            </div>
            <div class="row">
                <div class="col-md-12" style="text-align:left; font-weight:bold">
                    <asp:Label ID="lblDRTransactionSuccess" runat="server" Text="" Visible="false" ></asp:Label>
                </div> 
            </div>
            <div class="row">
                <div class="col-md-12" style="text-align:left; font-weight:bold">
                     <asp:Label ID="lblChargedAmount" runat="server" Text="" Visible="false" ></asp:Label>
                </div>
            </div>         
        </div> 
    </div>     
</asp:Content>
