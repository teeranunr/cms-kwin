﻿Public Class SiteMaster
    Inherits MasterPage
    Private Const AntiXsrfTokenKey As String = "__AntiXsrfToken"
    Private Const AntiXsrfUserNameKey As String = "__AntiXsrfUserName"
    Private _antiXsrfTokenValue As String
    Private intUserId As Integer

    Protected Sub Page_Init(sender As Object, e As EventArgs)
        ' The code below helps to protect against XSRF attacks
        Dim requestCookie = Request.Cookies(AntiXsrfTokenKey)
        Dim requestCookieGuidValue As Guid
        If requestCookie IsNot Nothing AndAlso Guid.TryParse(requestCookie.Value, requestCookieGuidValue) Then
            ' Use the Anti-XSRF token from the cookie
            _antiXsrfTokenValue = requestCookie.Value
            Page.ViewStateUserKey = _antiXsrfTokenValue
        Else
            ' Generate a new Anti-XSRF token and save to the cookie
            _antiXsrfTokenValue = Guid.NewGuid().ToString("N")
            Page.ViewStateUserKey = _antiXsrfTokenValue

            Dim responseCookie = New HttpCookie(AntiXsrfTokenKey) With { _
                 .HttpOnly = True, _
                 .Value = _antiXsrfTokenValue _
            }
            If FormsAuthentication.RequireSSL AndAlso Request.IsSecureConnection Then
                responseCookie.Secure = True
            End If
            Response.Cookies.[Set](responseCookie)
        End If

        AddHandler Page.PreLoad, AddressOf master_Page_PreLoad
    End Sub

    Protected Sub master_Page_PreLoad(sender As Object, e As EventArgs)
        If Not IsPostBack Then
            ' Set Anti-XSRF token
            ViewState(AntiXsrfTokenKey) = Page.ViewStateUserKey
            ViewState(AntiXsrfUserNameKey) = If(Context.User.Identity.Name, [String].Empty)
        Else
            ' Validate the Anti-XSRF token
            If DirectCast(ViewState(AntiXsrfTokenKey), String) <> _antiXsrfTokenValue OrElse DirectCast(ViewState(AntiXsrfUserNameKey), String) <> (If(Context.User.Identity.Name, [String].Empty)) Then
                Throw New InvalidOperationException("Validation of Anti-XSRF token failed.")
            End If
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim Cookie As HttpCookie
        Dim strUsrId As String
        Dim intUserId As Int16
        Try
            Cookie = Request.Cookies("USR")
            strUsrId = Cookie.Item("USR_ID")
            If (Request.Cookies("USR") IsNot Nothing) Then
                If (Request.Cookies("USR")("USR_ID") IsNot Nothing) Then
                    btnLogout.Visible = True
                    intUserId = Request.Cookies("USR")("USR_ID")
                    If intUserId = 1 Or intUserId = 3 Then
                        import.Visible = True
                        lblHeader.Text = "CONTENT:"
                    ElseIf intUserId = 2 Then
                        lblHeader.Text = "Report:"
                        likSubscription.Visible = False
                        likTransaction.Visible = True
                        likTransactionPartner.Visible = True
                        likTransaction4194006.Visible = True
                        likTransService.Visible = False
                        likMonitor.Visible = True
                    ElseIf intUserId = 4 Then
                        lblHeader.Text = "CUSTOMER INFORMATION"
                        likAddCase.Visible = True
                        likViewCase.Visible = True
                    End If
                ElseIf (Request.Cookies("USR")("PTN_ID") IsNot Nothing) Then
                    btnLogout.Visible = True
                    intUserId = Request.Cookies("USR")("PTN_ID")
                    If intUserId = 2 Then
                        lblHeader.Text = "Case Isoftel:"
                    Else
                        lblHeader.Text = "Partner Report:"
                        likPartnerSubscription.Visible = True
                        likPartnerTransaction.Visible = True
                        likTransaction4194006.Visible = True
                        likPartnerTransService.Visible = True
                    End If
                    'likViewCase.Visible = True
                Else
                    lblHeader.Text = "CONTENT MANAGEMENT SYSTEM"
                    btnLogout.Visible = False
                    import.Visible = False
                    intUserId = 0
                End If
            Else
            End If
        Catch ex As Exception
            btnLogout.Visible = False
            import.Visible = False
        Finally

        End Try
        Dim url As String = Request.Url.AbsolutePath
        If intUserId = 0 Then
            If url = "/Account/login" Then

            Else
                Response.Redirect("/Account/login.aspx")
            End If
        End If
    End Sub

    Protected Sub Unnamed_LoggingOut(sender As Object, e As LoginCancelEventArgs)
        Context.GetOwinContext().Authentication.SignOut()
    End Sub

    Protected Sub btnLogout_Click(sender As Object, e As EventArgs) Handles btnLogout.Click
        If (Not Request.Cookies("USR") Is Nothing) Then
            Dim myCookie As HttpCookie
            myCookie = New HttpCookie("USR")
            myCookie.Expires = DateTime.Now.AddDays(-1D)
            Response.Cookies.Add(myCookie)
        End If
        Response.Redirect("/Account/login.aspx")
    End Sub
End Class