﻿Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.EntityFramework
Imports Microsoft.AspNet.Identity.Owin
Imports System.Web
Imports System.Web.UI
Imports Microsoft.Owin.Security
Imports System.Data.SqlClient
Partial Public Class Login
    Inherits Page
    Private strSql As String
    Private strUserName As String
    Private strPassword As String
    Private Objds As DataSet
    Private funCenter As New funCenter
    Public Cookie As HttpCookie
    Private intUsrId As Integer
    Private strName As String

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        'RegisterHyperLink.NavigateUrl = "Register"
        'OpenAuthLogin.ReturnUrl = Request.QueryString("ReturnUrl")
        'Dim returnUrl = HttpUtility.UrlEncode(Request.QueryString("ReturnUrl"))
        'If Not [String].IsNullOrEmpty(returnUrl) Then
        '    RegisterHyperLink.NavigateUrl += "?ReturnUrl=" & returnUrl
        'End If
    End Sub

    Protected Sub LogIn(sender As Object, e As EventArgs)
        If IsValid Then
            ' Validate the user password
            'Dim manager = New UserManager()
            'Dim user As ApplicationUser = manager.Find(UserName.Text, Password.Text)
            'If user IsNot Nothing Then
            '    IdentityHelper.SignIn(manager, user, RememberMe.Checked)
            '    IdentityHelper.RedirectToReturnUrl(Request.QueryString("ReturnUrl"), Response)
            'Else
            '    FailureText.Text = "Invalid username or password."
            '    ErrorMessage.Visible = True
            'End If
            strUserName = UserName.Text.Trim()
            strPassword = Password.Text.Trim()
            If strUserName <> "" And strPassword <> "" Then
                Try
                    Dim MyConn As SqlConnection
                    MyConn = funCenter.Connection()
                    If MyConn.State = ConnectionState.Closed Then
                        MyConn.Open()
                    End If
                    strSql = "SELECT TOP 1 USR_ID,USR_PWD " & _
                            " FROM USERLOGIN" & _
                            " WHERE USR_STATUS = 'Y' AND USR_NAME=@userName"
                    Dim command As New SqlCommand()
                    command.Connection = MyConn
                    command.CommandText = strSql
                    command.Parameters.Add(New SqlParameter("@userName", strUserName))
                    Dim reader As SqlDataReader = command.ExecuteReader()
                    If reader.Read() Then
                        If reader.GetString(1) = strPassword Then
                            intUsrId = reader.GetInt16(0)
                            strName = "Name"
                            Cookie = New HttpCookie("USR")
                            Cookie.Values("USR_ID") = intUsrId
                            Response.Cookies.Add(Cookie)
                            If intUsrId = 1 Or intUsrId = 3 Then
                                Response.Redirect("/log_menu.aspx?mnu_id=1")
                            ElseIf intUsrId = 2 Then
                                Response.Redirect("/log_menu.aspx?mnu_id=3")
                            ElseIf intUsrId = 4 Then
                                Response.Redirect("/log_menu.aspx?mnu_id=5")
                            End If
                        Else
                            FailureText.Text = "Invalid username or password."
                            ErrorMessage.Visible = True
                        End If
                    Else
                        If MyConn.State = ConnectionState.Open Then
                            MyConn.Close()
                        End If

                        MyConn = funCenter.ReportPartnerConnection()
                        If MyConn.State = ConnectionState.Closed Then
                            MyConn.Open()
                        End If
                        strSql = "SELECT TOP 1 PTN_ID,PTN_PASWORD" & _
                                   " FROM PARTNER" & _
                                   " WHERE PTN_USERNAME=@userName"
                        Dim command2 As New SqlCommand()
                        command2.Connection = MyConn
                        command2.CommandText = strSql
                        command2.Parameters.Add(New SqlParameter("@userName", strUserName))
                        Dim reader2 As SqlDataReader = command2.ExecuteReader()

                        If reader2.Read() Then
                            If reader2.GetString(1) = strPassword Then
                                intUsrId = reader2.GetInt16(0)
                                strName = "Name"
                                Cookie = New HttpCookie("USR")
                                Cookie.Values("PTN_ID") = intUsrId
                                Response.Cookies.Add(Cookie)
                                'Response.Write(intUsrId)
                                If intUsrId = 2 Then
                                    Response.Redirect("/log_menu.aspx?mnu_id=10")
                                Else
                                    Response.Redirect("/log_menu.aspx?mnu_id=6")
                                End If
                            Else
                                FailureText.Text = "Invalid username or password."
                                ErrorMessage.Visible = True
                            End If
                        Else
                            FailureText.Text = "Invalid username or password."
                            ErrorMessage.Visible = True
                        End If
                    End If
                Catch ex As Exception
                    Response.Write(ex.Message)
                End Try
            End If
        End If
        'End If
    End Sub
End Class
