﻿Imports System.IO
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports Excel
Imports System.Globalization

Public Class case_detail
    Inherits System.Web.UI.Page
    Private strSql As String
    Private funCenter As New funCenter
    Private MyConn As SqlConnection
    Private sqlTrans As SqlTransaction
    Private intUserId As Integer = 0
    Private intPtnId As Integer = 0
    Private strCultureInfo As CultureInfo = New CultureInfo("en-US")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Request.Cookies("USR") IsNot Nothing) Then
            If (Request.Cookies("USR")("USR_ID") IsNot Nothing) Then
                intUserId = Request.Cookies("USR")("USR_ID")
            Else
                If (Request.Cookies("USR")("PTN_ID") IsNot Nothing) Then
                    intPtnId = Request.Cookies("USR")("PTN_ID")
                Else

                End If
            End If
        Else

        End If
        If Not Page.IsPostBack Then
            cldStart.Visible = False
            cldEnd.Visible = False
            BindData()
        End If
    End Sub

    Public Sub BindData()
        Dim strWhere As String = ""
        Dim strStatus As String = ""
        Try
            Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("IsoftelConnection").ConnectionString)
            conn.Open()
            If txbStart.Text.Trim() <> "" And txbEnd.Text.Trim() <> "" Then
                strWhere = strWhere + " AND CREATE_DATE_TXT BETWEEN '" & txbStart.Text.Trim() & "' AND '" & txbEnd.Text.Trim() & "'"
            End If

            If ddlStatus.SelectedValue = "" Then
                strWhere = strWhere + " AND CIQ_STATUS IN(0,1,2)"
            Else
                strWhere = strWhere + " AND CIQ_STATUS IN(" + ddlStatus.SelectedValue + ")"
            End If

            If txbMobileNo.Text.Trim() <> "" Then
                strWhere = strWhere + " AND PHONE_NO =" + txbMobileNo.Text.Trim()
            End If

            strSql = "SELECT TOP 500 CIQ_ID,PHONE_NO,CIQ_DETAIL,CIQ_STATUS," & _
                    " ISNULL(EXCEL_FILE_ID,0) AS EXCEL_FILE_ID,ISNULL(VOICE_FILE_ID,0) AS VOICE_FILE_ID," & _
                    " RIGHT('00'+CAST(datepart(dd,CREATE_DATE) AS varchar(20)),2) + '-'+" & _
                    " RIGHT('00'+CAST(datepart(mm,CREATE_DATE) AS varchar(20)),2) + '-' +" & _
                    " CAST(datepart(yyyy,CREATE_DATE) AS varchar(20))  + ' ' + " & _
                    " RIGHT('00'+CAST(datepart(hh,CREATE_DATE) AS varchar(20)),2) + ':'+ " & _
                    " RIGHT('00'+CAST(datepart(mi,CREATE_DATE) AS varchar(20)),2) + ':' + " & _
                    " RIGHT('00'+CAST(datepart(ss,CREATE_DATE) AS varchar(20)),2) AS CREATE_DATE" & _
                    " FROM CASE_INQUIRY WHERE CIQ_ID IS NOT NULL " & strWhere & _
                    " ORDER BY CIQ_ID DESC"
            Dim da As New SqlDataAdapter(strSql, conn)
            Dim ds As New DataSet()
            da.Fill(ds, "CASE_INQUIRY")
            If ds.Tables("CASE_INQUIRY").Rows.Count > 0 Then
                GridView1.DataSource = ds.Tables("CASE_INQUIRY")
                GridView1.DataBind()
                GridView1.Visible = True
                'lblError.Visible = False
            Else
                'lblError.Visible = True
                'lblError.Text = "No Data!"
                GridView1.DataSource = Nothing
                GridView1.Visible = False
            End If
            conn.Close()
        Catch ex As Exception
            Response.Write(ex.Message.ToString)
        End Try
    End Sub



    Private Sub GridView1_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GridView1.RowCommand
        Dim bts As Button = TryCast(e.CommandSource, Button)
        Dim ciqId As Integer = Integer.Parse(e.CommandArgument.ToString())
        Dim gvRow As GridViewRow = DirectCast(DirectCast(e.CommandSource, Button).NamingContainer, GridViewRow)
        Dim phoneNo As Label = DirectCast(gvRow.FindControl("phoneNo"), Label)
        Dim strDate As String = DateTime.Now.ToString("yyyyMMddHHmmss")
        Dim FolderExcelPath As String = ConfigurationManager.AppSettings("CaseIsoftelExcelPath")
        Dim FolderVdoPath As String = ConfigurationManager.AppSettings("CaseIsoftelVdoPath")

        If e.CommandName.ToLower() = "uploadexcel" Then
            Dim fuExcel As FileUpload = TryCast(bts.FindControl("FileUploadExcel"), FileUpload)

            Dim upload As Boolean = True

            If fuExcel.HasFile Then
                Dim fleUpload As String = Path.GetExtension(fuExcel.FileName.ToString())
                Dim GenFileName As String = phoneNo.Text.Trim() + "_" + Left(strDate, 8) + "_" + Right(strDate, 6) + fleUpload.Trim().ToLower()
                If fleUpload.Trim().ToLower() = ".xls" Or fleUpload.Trim().ToLower() = ".xlsx" Then
                    fuExcel.SaveAs(Server.MapPath(FolderExcelPath + GenFileName))
                    'Dim uploadedFile As String = (Server.MapPath(FolderExcelPath + fuExcel.FileName.ToString()))
                    Try
                        MyConn = funCenter.IsoftelConnection()
                        If MyConn.State = ConnectionState.Closed Then
                            MyConn.Open()
                        End If
                        sqlTrans = MyConn.BeginTransaction
                        strSql = "INSERT INTO CASE_INQUIRY_FILE_UPLOAD(CIQ_ID,FILE_ORIGINAL_NAME,FILE_ACTUAL_NAME)" & _
                                " VALUES(@ciqId,@original,@actual); select SCOPE_IDENTITY();"
                        Dim insertContent As New SqlCommand()
                        insertContent.Connection = MyConn
                        insertContent.Transaction = sqlTrans
                        insertContent.CommandText = strSql
                        insertContent.Parameters.Add(New SqlParameter("@ciqId", ciqId))
                        insertContent.Parameters.Add(New SqlParameter("@original", fuExcel.FileName.ToString()))
                        insertContent.Parameters.Add(New SqlParameter("@actual", GenFileName))
                        Dim evelObj As Object = insertContent.ExecuteScalar()
                        Dim cfileId As Integer
                        If evelObj <> Nothing Then
                            cfileId = CInt(CDec(evelObj))
                        End If
                        sqlTrans.Commit()

                        sqlTrans = MyConn.BeginTransaction
                        strSql = "UPDATE CASE_INQUIRY SET EXCEL_FILE_ID = @fileId" & _
                                " WHERE CIQ_ID = @ciqId"
                        Dim updateContent As New SqlCommand()
                        updateContent.Connection = MyConn
                        updateContent.Transaction = sqlTrans
                        updateContent.CommandText = strSql
                        updateContent.Parameters.Add(New SqlParameter("@ciqId", ciqId))
                        updateContent.Parameters.Add(New SqlParameter("@fileId", cfileId))
                        updateContent.ExecuteNonQuery()
                        sqlTrans.Commit()
                    Catch ex As Exception
                        Response.Write(ex.Message)
                    Finally

                    End Try

                    If MyConn.State = ConnectionState.Open Then
                        MyConn.Close()
                    End If
                Else
                    ' Something to do?...
                    upload = False
                End If
                ' somthing to do?...
                If upload Then
                End If
                'Something to do?...
            Else
                lblerror.Visible = True
                lblerror.Text = "Incorrect file format!"
            End If
        ElseIf e.CommandName.ToLower() = "uploadvoice" Then
            Dim fuVdo As FileUpload = TryCast(bts.FindControl("FileUploadVoice"), FileUpload)
            Dim upload As Boolean = True

            If fuVdo.HasFile Then
                Dim fleUpload As String = Path.GetExtension(fuVdo.FileName.ToString())
                Dim GenFileName As String = phoneNo.Text.Trim() + "_" + Left(strDate, 8) + "_" + Right(strDate, 6) + fleUpload.Trim().ToLower()
                If fleUpload.Trim().ToLower() = ".mp4" Or fleUpload.Trim().ToLower() = ".wma" Or fleUpload.Trim().ToLower() = ".wav" Then
                    fuVdo.SaveAs(Server.MapPath(FolderVdoPath + fuVdo.FileName.ToString()))
                    Try
                        MyConn = funCenter.IsoftelConnection()
                        If MyConn.State = ConnectionState.Closed Then
                            MyConn.Open()
                        End If
                        sqlTrans = MyConn.BeginTransaction
                        strSql = "INSERT INTO CASE_INQUIRY_FILE_UPLOAD(CIQ_ID,FILE_ORIGINAL_NAME,FILE_ACTUAL_NAME)" & _
                                " VALUES(@ciqId,@original,@actual); select SCOPE_IDENTITY();"
                        Dim insertContent As New SqlCommand()
                        insertContent.Connection = MyConn
                        insertContent.Transaction = sqlTrans
                        insertContent.CommandText = strSql
                        insertContent.Parameters.Add(New SqlParameter("@ciqId", ciqId))
                        insertContent.Parameters.Add(New SqlParameter("@original", fuVdo.FileName.ToString()))
                        insertContent.Parameters.Add(New SqlParameter("@actual", GenFileName))
                        Dim evelObj As Object = insertContent.ExecuteScalar()
                        Dim cfileId As Integer
                        If evelObj <> Nothing Then
                            cfileId = CInt(CDec(evelObj))
                        End If
                        sqlTrans.Commit()

                        sqlTrans = MyConn.BeginTransaction
                        strSql = "UPDATE CASE_INQUIRY SET VOICE_FILE_ID = @fileId" & _
                                " WHERE CIQ_ID = @ciqId"
                        Dim updateContent As New SqlCommand()
                        updateContent.Connection = MyConn
                        updateContent.Transaction = sqlTrans
                        updateContent.CommandText = strSql
                        updateContent.Parameters.Add(New SqlParameter("@ciqId", ciqId))
                        updateContent.Parameters.Add(New SqlParameter("@fileId", cfileId))
                        updateContent.ExecuteNonQuery()
                        sqlTrans.Commit()
                    Catch ex As Exception
                        Response.Write(ex.Message)
                    Finally

                    End Try

                    If MyConn.State = ConnectionState.Open Then
                        MyConn.Close()
                    End If
                    If upload Then
                    End If
                End If
            Else
                lblerror.Visible = True
                lblerror.Text = "Incorrect file format!"
            End If
        ElseIf e.CommandName.ToLower() = "downloadexcel" Then
            Dim MyConn As SqlConnection
            MyConn = funCenter.IsoftelConnection()
            If MyConn.State = ConnectionState.Closed Then
                MyConn.Open()
            End If
            strSql = "SELECT TOP 1 ISNULL(FILE_ACTUAL_NAME,'') AS FILE_ACTUAL_NAME" & _
                    " FROM CASE_INQUIRY A, CASE_INQUIRY_FILE_UPLOAD B" & _
                    " WHERE A.EXCEL_FILE_ID = B.CFILE_ID" & _
                    " AND A.CIQ_ID = @ciqId"
            Dim command As New SqlCommand()
            command.Connection = MyConn
            command.CommandText = strSql
            command.Parameters.Add(New SqlParameter("@ciqId", ciqId))
            Dim reader As SqlDataReader = command.ExecuteReader()
            If reader.Read() Then
                Dim fileName As String = reader.GetString(0)
                Dim filePath As String = Server.MapPath(FolderExcelPath + fileName)
                Response.ContentType = ContentType
                Response.AppendHeader("Content-Disposition", ("attachment; filename=" + Path.GetFileName(filePath)))
                Response.WriteFile(filePath)
                Response.End()
            End If
        ElseIf e.CommandName.ToLower() = "downloadvoice" Then
            Dim MyConn As SqlConnection
            MyConn = funCenter.IsoftelConnection()
            If MyConn.State = ConnectionState.Closed Then
                MyConn.Open()
            End If
            strSql = "SELECT TOP 1 ISNULL(FILE_ACTUAL_NAME,'') AS FILE_ACTUAL_NAME" & _
                    " FROM CASE_INQUIRY A, CASE_INQUIRY_FILE_UPLOAD B" & _
                    " WHERE A.VOICE_FILE_ID = B.CFILE_ID" & _
                    " AND A.CIQ_ID = @ciqId"
            Dim command As New SqlCommand()
            command.Connection = MyConn
            command.CommandText = strSql
            command.Parameters.Add(New SqlParameter("@ciqId", ciqId))
            Dim reader As SqlDataReader = command.ExecuteReader()
            If reader.Read() Then
                Dim fileName As String = reader.GetString(0)
                Dim filePath As String = Server.MapPath(FolderExcelPath + fileName)
                Response.ContentType = ContentType
                Response.AppendHeader("Content-Disposition", ("attachment; filename=" + Path.GetFileName(filePath)))
                Response.WriteFile(filePath)
                Response.End()
            End If
        ElseIf e.CommandName.ToLower() = "removeexcel" Then
            MyConn = funCenter.IsoftelConnection()
            If MyConn.State = ConnectionState.Closed Then
                MyConn.Open()
            End If
            strSql = "SELECT TOP 1 A.EXCEL_FILE_ID,ISNULL(FILE_ACTUAL_NAME,'') AS FILE_ACTUAL_NAME" & _
                                " FROM CASE_INQUIRY A, CASE_INQUIRY_FILE_UPLOAD B" & _
                                " WHERE A.EXCEL_FILE_ID = B.CFILE_ID" & _
                                " AND A.CIQ_ID = @ciqId"
            Dim command As New SqlCommand()
            command.Connection = MyConn
            command.CommandText = strSql
            command.Parameters.Add(New SqlParameter("@ciqId", ciqId))
            Dim reader As SqlDataReader = command.ExecuteReader()
            Dim cfileId As Integer = 0
            Dim filePath As String = ""
            If reader.Read() Then
                cfileId = reader.GetInt32(0)
                filePath = Server.MapPath(FolderExcelPath + reader.GetString(1))
            Else

            End If
            If MyConn.State = ConnectionState.Open Then
                MyConn.Close()
            End If

            If cfileId > 0 Then
                If MyConn.State = ConnectionState.Closed Then
                    MyConn.Open()
                End If
                sqlTrans = MyConn.BeginTransaction
                strSql = "UPDATE CASE_INQUIRY SET EXCEL_FILE_ID = NULL" & _
                        " WHERE CIQ_ID = @ciqId AND EXCEL_FILE_ID = @fileId"
                Dim updateContent As New SqlCommand()
                updateContent.Connection = MyConn
                updateContent.Transaction = sqlTrans
                updateContent.CommandText = strSql
                updateContent.Parameters.Add(New SqlParameter("@ciqId", ciqId))
                updateContent.Parameters.Add(New SqlParameter("@fileId", cfileId))
                updateContent.ExecuteNonQuery()
                sqlTrans.Commit()

                sqlTrans = MyConn.BeginTransaction
                strSql = "DELETE CASE_INQUIRY_FILE_UPLOAD" & _
                        " WHERE CIQ_ID = @ciqId AND CFILE_ID = @fileId"
                Dim deleteContent As New SqlCommand()
                deleteContent.Connection = MyConn
                deleteContent.Transaction = sqlTrans
                deleteContent.CommandText = strSql
                deleteContent.Parameters.Add(New SqlParameter("@ciqId", ciqId))
                deleteContent.Parameters.Add(New SqlParameter("@fileId", cfileId))
                deleteContent.ExecuteNonQuery()
                sqlTrans.Commit()
            Else

            End If
            Try
                File.Delete(Server.MapPath(filePath))
            Catch ex As Exception
                ex.Message.ToString()
            End Try

            If MyConn.State = ConnectionState.Open Then
                MyConn.Close()
            End If
        ElseIf e.CommandName.ToLower() = "removevoice" Then
            MyConn = funCenter.IsoftelConnection()
            If MyConn.State = ConnectionState.Closed Then
                MyConn.Open()
            End If
            strSql = "SELECT TOP 1 A.VOICE_FILE_ID,ISNULL(FILE_ACTUAL_NAME,'') AS FILE_ACTUAL_NAME" & _
                    " FROM CASE_INQUIRY A, CASE_INQUIRY_FILE_UPLOAD B" & _
                    " WHERE A.VOICE_FILE_ID = B.CFILE_ID" & _
                    " AND A.CIQ_ID = @ciqId"
            Dim command As New SqlCommand()
            command.Connection = MyConn
            command.CommandText = strSql
            command.Parameters.Add(New SqlParameter("@ciqId", ciqId))
            Dim reader As SqlDataReader = command.ExecuteReader()
            Dim cfileId As Integer = 0
            Dim filePath As String = ""
            If reader.Read() Then
                cfileId = reader.GetInt32(0)
                filePath = Server.MapPath(FolderExcelPath + reader.GetString(1))
            Else

            End If
            If MyConn.State = ConnectionState.Open Then
                MyConn.Close()
            End If

            If cfileId > 0 Then
                If MyConn.State = ConnectionState.Closed Then
                    MyConn.Open()
                End If
                sqlTrans = MyConn.BeginTransaction
                strSql = "UPDATE CASE_INQUIRY SET VOICE_FILE_ID = NULL" & _
                        " WHERE CIQ_ID = @ciqId AND VOICE_FILE_ID = @fileId"
                Dim updateContent As New SqlCommand()
                updateContent.Connection = MyConn
                updateContent.Transaction = sqlTrans
                updateContent.CommandText = strSql
                updateContent.Parameters.Add(New SqlParameter("@ciqId", ciqId))
                updateContent.Parameters.Add(New SqlParameter("@fileId", cfileId))
                updateContent.ExecuteNonQuery()
                sqlTrans.Commit()

                sqlTrans = MyConn.BeginTransaction
                strSql = "DELETE CASE_INQUIRY_FILE_UPLOAD" & _
                        " WHERE CIQ_ID = @ciqId AND CFILE_ID = @fileId"
                Dim deleteContent As New SqlCommand()
                deleteContent.Connection = MyConn
                deleteContent.Transaction = sqlTrans
                deleteContent.CommandText = strSql
                deleteContent.Parameters.Add(New SqlParameter("@ciqId", ciqId))
                deleteContent.Parameters.Add(New SqlParameter("@fileId", cfileId))
                deleteContent.ExecuteNonQuery()
                sqlTrans.Commit()
            Else

            End If
            Try
                File.Delete(Server.MapPath(filePath))
            Catch ex As Exception

            End Try

            If MyConn.State = ConnectionState.Open Then
                MyConn.Close()
            End If
        End If

        If e.CommandName.ToLower() = "uploadexcel" Or e.CommandName.ToLower() = "uploadvoice" Or e.CommandName.ToLower() = "removeexcel" Or e.CommandName.ToLower() = "removevoice" Then
            MyConn = funCenter.IsoftelConnection()
            If MyConn.State = ConnectionState.Closed Then
                MyConn.Open()
            End If
            strSql = "SELECT TOP 1 ISNULL(EXCEL_FILE_ID,0) AS EXCEL_FILE_ID,ISNULL(VOICE_FILE_ID,0) AS VOICE_FILE_ID" & _
                    " FROM CASE_INQUIRY WHERE CIQ_ID = @ciqId"
            Dim command As New SqlCommand()
            command.Connection = MyConn
            command.CommandText = strSql
            command.Parameters.Add(New SqlParameter("@ciqId", ciqId))
            Dim reader As SqlDataReader = command.ExecuteReader()

            Dim excelfile As String = 0
            Dim voicefile As String = 0

            If reader.Read() Then
                excelfile = reader.GetInt32(0)
                voicefile = reader.GetInt32(1)
            End If

            If MyConn.State = ConnectionState.Open Then
                MyConn.Close()
            End If

            Dim status As Int16
            If excelfile > 0 And voicefile > 0 Then
                status = 0
            ElseIf excelfile = 0 And voicefile = 0 Then
                status = 2
            Else
                status = 1
            End If
            If MyConn.State = ConnectionState.Closed Then
                MyConn.Open()
            End If
            sqlTrans = MyConn.BeginTransaction
            strSql = "UPDATE CASE_INQUIRY SET CIQ_STATUS = @status" & _
                    " WHERE CIQ_ID = @ciqId"
            Dim updateContent As New SqlCommand()
            updateContent.Connection = MyConn
            updateContent.Transaction = sqlTrans
            updateContent.CommandText = strSql
            updateContent.Parameters.Add(New SqlParameter("@ciqId", ciqId))
            updateContent.Parameters.Add(New SqlParameter("@status", status))
            updateContent.ExecuteNonQuery()
            sqlTrans.Commit()

            If MyConn.State = ConnectionState.Open Then
                MyConn.Close()
            End If
        End If

        BindData()
    End Sub

    Private Sub GridView1_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GridView1.RowDataBound
        Dim checkFileExcel As String = ""
        Dim checkFileVoice As String = ""
        Dim imgStatus As Image = DirectCast(e.Row.FindControl("imgStatus"), Image)
        Dim fileNameExcel As Label = DirectCast(e.Row.FindControl("lblFileNameExcel"), Label)
        Dim btnRemoveExcel As Button = DirectCast(e.Row.FindControl("btnRemoveExcel"), Button)
        Dim btnUploadExcel As Button = DirectCast(e.Row.FindControl("btnUploadExcel"), Button)
        Dim btnDownloadExcel As Button = DirectCast(e.Row.FindControl("btnDownloadExcel"), Button)
        Dim FileUploadExcel As FileUpload = DirectCast(e.Row.FindControl("FileUploadExcel"), FileUpload)


        Dim fileNameVoice As Label = DirectCast(e.Row.FindControl("lblFileNameVoice"), Label)
        Dim btnRemoveVoice As Button = DirectCast(e.Row.FindControl("btnRemoveVoice"), Button)
        Dim btnUploadVoice As Button = DirectCast(e.Row.FindControl("btnUploadVoice"), Button)
        Dim btnDownloadVoice As Button = DirectCast(e.Row.FindControl("btnDownloadVoice"), Button)
        Dim FileUploadVoice As FileUpload = DirectCast(e.Row.FindControl("FileUploadVoice"), FileUpload)

        If Not fileNameExcel Is Nothing Then
            If fileNameExcel.Text <> "" Then
                If intUserId = 4 Then
                    btnRemoveExcel.Visible = False
                Else
                    btnRemoveExcel.Visible = True
                End If
                If intPtnId = 2 Then
                    btnDownloadExcel.Visible = False
                Else
                    btnDownloadExcel.Visible = True
                End If

                btnUploadExcel.Visible = False
                FileUploadExcel.Visible = False
                checkFileExcel = "A"
            Else
                btnRemoveExcel.Visible = False
                btnDownloadExcel.Visible = False
                If intUserId = 4 Then
                    btnUploadExcel.Visible = False
                    FileUploadExcel.Visible = False
                Else
                    btnUploadExcel.Visible = True
                    FileUploadExcel.Visible = True
                End If
                checkFileExcel = "I"
            End If
        End If

        If Not fileNameVoice Is Nothing Then
            If fileNameVoice.Text <> "" Then
                If intUserId = 4 Then
                    btnRemoveVoice.Visible = False
                Else
                    btnRemoveVoice.Visible = True
                End If
                If intPtnId = 2 Then
                    btnDownloadVoice.Visible = False
                Else
                    btnDownloadVoice.Visible = True
                End If
                btnUploadVoice.Visible = False
                FileUploadVoice.Visible = False
                checkFileVoice = "A"
            Else
                btnRemoveVoice.Visible = False
                btnDownloadVoice.Visible = False
                If intUserId = 4 Then
                    btnUploadVoice.Visible = False
                    FileUploadVoice.Visible = False
                Else
                    btnUploadVoice.Visible = True
                    FileUploadVoice.Visible = True
                End If
                checkFileVoice = "I"
            End If
        End If

        If Not imgStatus Is Nothing Then
            If checkFileExcel = "A" And checkFileVoice = "A" Then
                imgStatus.ImageUrl = "images/green.png"
            ElseIf checkFileExcel = "I" And checkFileVoice = "I" Then
                imgStatus.ImageUrl = "images/red.png"
            Else
                imgStatus.ImageUrl = "images/yellow.png"
            End If
        End If
    End Sub

    Function GetFileName(cfileId As Integer, type As String) As String
        Dim MyConn As SqlConnection
        MyConn = funCenter.IsoftelConnection()
        If MyConn.State = ConnectionState.Closed Then
            MyConn.Open()
        End If
        If cfileId > 0 Then
            strSql = "SELECT TOP 1 ISNULL(FILE_ORIGINAL_NAME,'') AS FILE_ORIGINAL_NAME," & _
                    " RIGHT('00'+CAST(datepart(dd,FILE_CREATE_DATE) AS varchar(20)),2) + '-'+" & _
                    " RIGHT('00'+CAST(datepart(mm,FILE_CREATE_DATE) AS varchar(20)),2) + '-' +" & _
                    " CAST(datepart(yyyy,FILE_CREATE_DATE) AS varchar(20))  + ' ' + " & _
                    " RIGHT('00'+CAST(datepart(hh,FILE_CREATE_DATE) AS varchar(20)),2) + ':'+ " & _
                    " RIGHT('00'+CAST(datepart(mi,FILE_CREATE_DATE) AS varchar(20)),2) + ':' + " & _
                    " RIGHT('00'+CAST(datepart(ss,FILE_CREATE_DATE) AS varchar(20)),2) AS CREATE_DATE" & _
                    " FROM CASE_INQUIRY_FILE_UPLOAD" & _
                    " WHERE CFILE_ID = @cfileId"
            Dim command As New SqlCommand()
            command.Connection = MyConn
            command.CommandText = strSql
            command.Parameters.Add(New SqlParameter("@cfileId", cfileId))
            Dim reader As SqlDataReader = command.ExecuteReader()
            If reader.Read() Then
                If type = "excel" Then
                    Return "File Name: " + reader.GetString(0)
                ElseIf type = "voice" Then
                    Return "File Name: " + reader.GetString(0)
                ElseIf type = "create_date" Then
                    Return "Create: " + reader.GetString(1)
                Else
                    Return Nothing
                End If
            Else
                Return Nothing
            End If
        Else
            Return Nothing
        End If
    End Function

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        BindData()
    End Sub

    Protected Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click
        txbMobileNo.Text = ""
        txbStart.Text = ""
        txbEnd.Text = ""
        ddlStatus.SelectedValue = ""
        lblerror.Visible = False
        lblerror.Text = ""
        BindData()
    End Sub

    Protected Sub btnStart_Click(sender As Object, e As EventArgs) Handles btnStart.Click
        cldStart.Visible = True
    End Sub

    Protected Sub btnEnd_Click(sender As Object, e As EventArgs) Handles btnEnd.Click
        cldEnd.Visible = True
    End Sub

    Protected Sub cldStart_SelectionChanged(sender As Object, e As EventArgs) Handles cldStart.SelectionChanged
        cldStart.Visible = False
        cldEnd.Visible = False
        txbStart.Text = cldStart.SelectedDate.ToString("yyyyMMdd", strCultureInfo)
    End Sub

    Protected Sub cldEnd_SelectionChanged(sender As Object, e As EventArgs) Handles cldEnd.SelectionChanged
        cldStart.Visible = False
        cldEnd.Visible = False
        txbEnd.Text = cldEnd.SelectedDate.ToString("yyyyMMdd", strCultureInfo)
    End Sub
End Class