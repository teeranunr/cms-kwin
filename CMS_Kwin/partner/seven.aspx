﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="seven.aspx.vb" Inherits="CMS_Kwin.seven" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<div class="container">
    <div class="row">
      <div class="col-md-12" style="height:10px;" >

      </div>  
    </div> 
    <div class="row">
        <div class="col-md-4" style="text-align:right">
            <asp:Label ID="Label2" runat="server" Text="START DATE: "></asp:Label>
            <asp:TextBox ID="txbStart" runat="server"></asp:TextBox>
            <asp:Button ID="btnStart" runat="server" Text="..." />
            <div id="divCalendar" style="POSITION: absolute; z-index:10; left: 50%; top: 50%; visibility: visible;">
                <asp:Calendar ID="cldStart" runat="server" BackColor="White"></asp:Calendar>                 
            </div> 
        </div>
        <div class="col-md-4" style="text-align:right">
             <asp:TextBox ID="txbMobileNo" runat="server"></asp:TextBox>
        </div>
       <div class="col-md-4" style="text-align:right">
             <asp:TextBox ID="txbBillNo" runat="server"></asp:TextBox>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12" style="text-align:center">
            <asp:Button ID="btnSearch" runat="server" Text="SEARCH" CssClass="btn btn-default"/>
        </div> 
    </div>
    <div class="row" style="height:10px">
       
    </div>
    <div class="row">
        <div class="col-md-12" style="text-align:center">
            <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
        </div> 
    </div>
    <div class="row">
      <div class="col-md-12" style="height:10px;" >

      </div>  
    </div>
</div>
            <asp:GridView ID="GridView1" runat="server" Width="100%" AutoGenerateColumns="False" 
                HeaderStyle-BackColor="Black" HeaderStyle-ForeColor="White" Visible="false" 
                ShowFooter="true" FooterStyle-BackColor="Black" FooterStyle-ForeColor="White" OnDataBound="GridView1_DataBound"
                FooterStyle-HorizontalAlign="Right">
                <Columns>
                    <asp:BoundField DataField="YYYYMMDD" HeaderText="Date" SortExpression="YYYYMMDD" ItemStyle-Width="20%" ItemStyle-HorizontalAlign="Center">                    
                        <ItemStyle HorizontalAlign="Center" Width="15%"></ItemStyle>
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Service" ItemStyle-Width="35%" ItemStyle-HorizontalAlign="left">
                        <itemtemplate>
					        <%# GetServiceName(Container.DataItem("SV_ID"))%>
				        </itemtemplate>
                        <ItemStyle HorizontalAlign="Left" Width="25%"></ItemStyle>
                    </asp:TemplateField>
                    <asp:BoundField DataField="MSISDN" HeaderText="Subscription" ItemStyle-Width="15%" 
                        ItemStyle-HorizontalAlign="right" DataFormatString="{0:###,##0}">
                        <ItemStyle HorizontalAlign="Right" Width="15%"></ItemStyle>
                    </asp:BoundField> 
                    <asp:BoundField DataField="BILL_NO" HeaderText="Cancel" ItemStyle-Width="15%" 
                        ItemStyle-HorizontalAlign="right" DataFormatString="{0:###,##0}">
                        <ItemStyle HorizontalAlign="Right" Width="15%"></ItemStyle>
                    </asp:BoundField> 
                    <asp:BoundField DataField="CARETE_DATE" HeaderText="Active" ItemStyle-Width="15%" 
                        ItemStyle-HorizontalAlign="right" DataFormatString="{0:###,##0}">
                        <ItemStyle HorizontalAlign="Right" Width="15%"></ItemStyle>
                    </asp:BoundField>                   
                </Columns>
            </asp:GridView>
</asp:Content>
