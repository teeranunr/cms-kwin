﻿Imports System.Globalization

Public Class seven
    Inherits System.Web.UI.Page
    Private funCenter As New funCenter
    Private objDs As DataSet
    Private strStartDate As String
    Private strEndDate As String
    Private strStartTime As String
    Private strStopTime As String
    Private strSvId As String
    Private strSql As String
    Private strWhere As String

    Private intUserId As Integer
    Private strCultureInfo As CultureInfo = New CultureInfo("en-US")
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        strSvId = Request.QueryString("sv_id")
        If (Request.Cookies("USR") IsNot Nothing) Then
            If (Request.Cookies("USR")("PTN_ID") IsNot Nothing) Then
                intUserId = Request.Cookies("USR")("PTN_ID")
            End If
        End If

        If Not IsPostBack() Then
            cldStart.Visible = False
            Dim dateNow As DateTime = DateTime.Now.AddDays(-1)
            txbStart.Text = dateNow.Year & Right("0" & dateNow.Month, 2) & Right("0" & dateNow.Day, 2)
        End If
        BindData()
    End Sub

    Sub BindData()
        Dim strBillNo As String = ""
        Dim strMobileNo As String = ""
        Dim strDate As String = ""
        Dim strWhere As String = ""
        strBillNo = txbBillNo.Text.Trim()
        strMobileNo = txbMobileNo.Text.Trim()
        strDate = txbStart.Text.Trim()
        If strBillNo <> "" Then
            strMobileNo = "66" & Right(strMobileNo, 9)
            strWhere = strWhere & " AND MSISDN = '" & strMobileNo & "'"
        End If

        If strBillNo <> "" Then
            strWhere = strWhere & " AND BILL_NO = '" & strBillNo & "'"
        End If

        If strDate <> "" Then
            strWhere = strWhere & " AND CREATE_DATE_TXT = '" & strDate & "'"
        End If

        strSql = "SELECT SV_ID,MSISDN,BILL_NO,CREATE_DATE_TXT" & _
                " FROM SEVEN_BILL" & _
                " WHERE MSISDN <> '' " & strWhere
        objDs = funCenter.QueryData(strSql, "DATA", "", "", "", "azu")
        If objDs.Tables("DATA").Rows.Count > 0 Then
            GridView1.DataSource = objDs.Tables("DATA")
            GridView1.DataBind()
            GridView1.Visible = True
            lblError.Visible = False
        Else
            GridView1.DataSource = Nothing
            GridView1.Visible = False
            lblError.Text = "No Data!"
            lblError.Visible = True
        End If
    End Sub

    Function GetServiceName(intSvId As Int16) As String
        Return funCenter.GetServiceName(intSvId)
    End Function
End Class