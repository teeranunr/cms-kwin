﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="4194006.aspx.vb" Inherits="CMS_Kwin._4194006" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
        <div class="row">
      <div class="col-md-12" style="height:10px;" >

      </div>  
    </div> 
    <div class="row">
        <div class="col-xs-12 col-md-12 col-sm-12 text-center" style="margin-top:10px;">
            <asp:Label ID="Label2" runat="server" Text="START DATE: "></asp:Label>
            <asp:TextBox ID="txbStart" runat="server"></asp:TextBox>
            <asp:Button ID="btnStart" runat="server" Text="..." />
            <div id="divCalendar" style="POSITION: absolute; z-index:10; left: 30%; top: 50%; visibility: visible;">
                <asp:Calendar ID="cldStart" runat="server" BackColor="White"></asp:Calendar>                 
            </div>&nbsp;&nbsp; 
            <asp:Label ID="Label1" runat="server" Text="END DATE: "></asp:Label>
            <asp:TextBox ID="txbEnd" runat="server" ></asp:TextBox>
            <asp:Button ID="btnEnd" runat="server" Text="..." />
            <div id="div1" style="POSITION:absolute; z-index:10; left:50%; top:50%; visibility:visible;">
                <asp:Calendar ID="cldEnd" runat="server" BackColor="White"></asp:Calendar>  
            </div> 
        </div>
    </div>
    <br />
        <div class="row">
        <div class="col-md-12" style="text-align:center">
            <asp:Button ID="btnSearch" runat="server" Text="SEARCH" CssClass="btn btn-default"/>
        </div> 
    </div>
    <div class="row">
        <div class="col-md-12" style="text-align:center">
            <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
        </div> 
    </div>        <div class="row">
      <div class="col-md-12" style="height:10px;" >

      </div>  
    </div>
            <div class="row">
        <div class="col-md-12" style="text-align:right">
            <asp:Button ID="btnExportExcel" runat="server" Text="EXPORT" Visible="false" CssClass="btn btn-default"/>
        </div> 
    </div>
            <div class="row">
      <div class="col-md-12" style="height:10px;" >

      </div>  
    </div>
    <asp:GridView ID="GridView" runat="server" Width="100%" AutoGenerateColumns="False" 
                HeaderStyle-BackColor="Black" HeaderStyle-ForeColor="White" Visible="false" 
                ShowFooter="true" FooterStyle-BackColor="Black" FooterStyle-ForeColor="White"
                FooterStyle-HorizontalAlign="Right">
                <Columns>
                    <asp:BoundField DataField="YYYYMMDD" HeaderText="Date" SortExpression="YYYYMMDD" ItemStyle-HorizontalAlign="Center">                    
                        <ItemStyle HorizontalAlign="Center" Width="20%"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="TOTAL_TRANS" HeaderText="Transaction Gen" ItemStyle-HorizontalAlign="right" DataFormatString="{0:###,##0}">
                        <ItemStyle HorizontalAlign="Right" Width="10%"></ItemStyle>
                    </asp:BoundField> 
                    <asp:BoundField DataField="TOTAL_SENT_TRANS" HeaderText="Transaction Sent (Include Retry)" ItemStyle-HorizontalAlign="right" DataFormatString="{0:###,##0}">
                        <ItemStyle HorizontalAlign="Right" Width="10%"></ItemStyle>
                    </asp:BoundField> 
                    <asp:BoundField DataField="SUCCESS_TRANS" HeaderText="Success" ItemStyle-HorizontalAlign="right" DataFormatString="{0:###,##0}">
                        <ItemStyle HorizontalAlign="Right" Width="10%"></ItemStyle>
                    </asp:BoundField> 
                    <asp:BoundField DataField="TOTAL_AMOUNT" HeaderText="Charged Amount (Baht)" ItemStyle-HorizontalAlign="right" DataFormatString="{0:###,##0}">
                        <ItemStyle HorizontalAlign="Right" Width="10%"></ItemStyle>
                    </asp:BoundField> 
                    <asp:BoundField DataField="SUCCESS_RATE" HeaderText="Success Rate" ItemStyle-HorizontalAlign="right">
                        <ItemStyle HorizontalAlign="Right" Width="10%"></ItemStyle>
                    </asp:BoundField>                    
                </Columns>
            </asp:GridView>
</asp:Content>
