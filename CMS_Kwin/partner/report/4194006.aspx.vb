﻿Imports System.Globalization
Imports System.IO
Public Class _4194006
    Inherits System.Web.UI.Page
    Private strSql As String
    Private funCenter As New funCenter
    Private objDs As DataSet
    Private strStartDate As String
    Private strEndDate As String
    Private intUserId As Integer
    Private total1 As Int32 = 0
    Private total2 As Int32 = 0
    Private total3 As Int32 = 0
    Private total4 As Int32 = 0
    Private strCultureInfo As CultureInfo = New CultureInfo("en-US")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Request.Cookies("USR") IsNot Nothing) Then
            If (Request.Cookies("USR")("PTN_ID") IsNot Nothing) Then
                intUserId = Request.Cookies("USR")("PTN_ID")
            End If
        End If

        If Not IsPostBack() Then
            cldStart.Visible = False
            cldEnd.Visible = False
            Dim dateNow As DateTime = DateTime.Now.AddDays(-1)
            txbStart.Text = dateNow.Year & Right("0" & dateNow.Month, 2) & Right("0" & dateNow.Day, 2)
            txbEnd.Text = dateNow.Year & Right("0" & dateNow.Month, 2) & Right("0" & dateNow.Day, 2)
            BindData()
        End If
    End Sub

    Sub BindData()
        Try
            cldStart.Visible = False
            cldEnd.Visible = False
            strSql = "SELECT TOP 100 YYYYMMDD,SUM(ISNULL(TOTAL_TRANS,0)) AS TOTAL_TRANS," &
                    " SUM(ISNULL(TOTAL_SENT_TRANS,0)) AS TOTAL_SENT_TRANS, SUM(ISNULL(SUCCESS_TRANS,0)) AS SUCCESS_TRANS," &
                    " SUM(ISNULL(TOTAL_AMOUNT,0)) AS TOTAL_AMOUNT," &
                    " Format(CONVERT(FLOAT,SUM(ISNULL(success_trans,0))) * 100 / SUM(ISNULL(TOTAL_TRANS,0)),'##.##') as SUCCESS_RATE" &
                    " FROM REP_TRANSACTION_PARTNER_2" &
                    " WHERE YYYYMMDD BETWEEN '" & txbStart.Text.Trim() & "' AND '" & txbEnd.Text.Trim() & "' AND SV_ID IN(SELECT SV_ID FROM [VREPORT_2].[dbo].PARTNER_SERVICE WHERE APP_ID IN('006'))" &
                    " GROUP BY YYYYMMDD" &
                    " ORDER BY YYYYMMDD"
            objDs = funCenter.QueryData(strSql, "DATA", "", "", "", "report_partner")
            If objDs.Tables("DATA").Rows.Count > 0 Then
                GridView.DataSource = objDs.Tables("DATA")
                GridView.DataBind()
                GridView.Visible = True
                lblError.Visible = False
                btnExportExcel.Visible = True
            Else
                GridView.DataSource = Nothing
                GridView.Visible = False
                lblError.Text = "No Data!"
                lblError.Visible = True
                btnExportExcel.Visible = False
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        BindData()
    End Sub
    Protected Sub btnStart_Click(sender As Object, e As EventArgs) Handles btnStart.Click
        cldStart.Visible = True
    End Sub

    Protected Sub btnEnd_Click(sender As Object, e As EventArgs) Handles btnEnd.Click
        cldEnd.Visible = True
    End Sub

    Protected Sub cldStart_SelectionChanged(sender As Object, e As EventArgs) Handles cldStart.SelectionChanged
        cldStart.Visible = False
        cldEnd.Visible = False
        txbStart.Text = cldStart.SelectedDate.ToString("yyyyMMdd", strCultureInfo)
        BindData()
    End Sub

    Protected Sub cldEnd_SelectionChanged(sender As Object, e As EventArgs) Handles cldEnd.SelectionChanged
        cldStart.Visible = False
        cldEnd.Visible = False
        txbEnd.Text = cldEnd.SelectedDate.ToString("yyyyMMdd", strCultureInfo)
        BindData()
    End Sub

    Protected Sub btnExportExcel_Click(sender As Object, e As EventArgs) Handles btnExportExcel.Click
        Dim sw As New StringWriter()
        Dim htw As New HtmlTextWriter(sw)
        Dim frm As New HtmlForm()
        Dim strFileName As String
        Dim strDate As String
        Dim strMonth As String
        Dim strYear As String
        If (Len(DateTime.Now.Day.ToString) = 1) Then
            strDate = "0" & DateTime.Now.Day.ToString
        Else
            strDate = DateTime.Now.Day.ToString
        End If
        If (Len(DateTime.Now.Month.ToString) = 1) Then
            strMonth = "0" & DateTime.Now.Month.ToString
        Else
            strMonth = DateTime.Now.Month.ToString
        End If
        strYear = DateTime.Now.Year.ToString

        strFileName = strYear & strMonth & strDate & Right("0" & DateTime.Now.Hour.ToString, 2) & Right("0" & DateTime.Now.Minute.ToString, 2) & Right("0" & DateTime.Now.Second.ToString, 2)
        Response.Clear()
        Response.ClearHeaders()
        Response.ClearContent()
        Response.AddHeader("content-disposition", "attachment;filename=" & strFileName & ".xls")
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.ContentEncoding = Encoding.UTF8
        Response.Charset = Encoding.UTF8.EncodingName
        Response.ContentType = "application/vnd.ms-excel"

        frm.Controls.Add(GridView)
        GridView.HeaderStyle.BackColor = Drawing.Color.White
        GridView.HeaderStyle.ForeColor = Drawing.Color.Black
        GridView.AlternatingRowStyle.BackColor = Drawing.Color.White
        GridView.BorderColor = Drawing.Color.Black
        GridView.BorderStyle = BorderStyle.Solid
        GridView.FooterStyle.BackColor = Drawing.Color.White
        GridView.FooterStyle.ForeColor = Drawing.Color.Black
        GridView.RenderControl(htw)

        Response.Write(sw.ToString())
        Response.End()
    End Sub

    Private Sub GridView_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GridView.RowDataBound
        Dim gvr As GridViewRow = e.Row
        If gvr.RowType = DataControlRowType.DataRow Then
            total1 += Replace(e.Row.Cells(1).Text, ",", "")
            total2 += Replace(e.Row.Cells(2).Text, ",", "")
            total3 += Replace(e.Row.Cells(3).Text, ",", "")
            total4 += Replace(e.Row.Cells(4).Text, ",", "")
        ElseIf gvr.RowType = DataControlRowType.Footer Then
            e.Row.Cells(0).Text = "Summary"
            e.Row.Cells(1).Text = FormatNumber(total1)
            e.Row.Cells(2).Text = FormatNumber(total2)
            e.Row.Cells(3).Text = FormatNumber(total3)
            e.Row.Cells(4).Text = FormatNumber(total4)
        End If
    End Sub
End Class