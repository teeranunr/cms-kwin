﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="case.aspx.vb" Inherits="CMS_Kwin._case" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <br />
    <div class="row">
        <div class="col-sm-4 col-md-4 label-right">
            <asp:Label ID="Label1" runat="server" Text="MOBILE NO: " Font-Bold="true"></asp:Label>
        </div>
        <div class="col-sm-8 col-md-8" style="text-align:left">
            <asp:TextBox ID="txbMobileNo" runat="server" CssClass="form-control" ></asp:TextBox>
        </div>
    </div>
    <br /> 
    <div class="row">
        <div class="col-sm-4 col-md-4 label-right">
            <asp:Label ID="Label2" runat="server" Text=" DETAIL: " Font-Bold="true" ></asp:Label>
        </div>
        <div class="col-sm-8 col-md-8 textarea" style="text-align:left;">
            <asp:TextBox ID="txbDetail" runat="server" TextMode="MultiLine" CssClass="form-control" Height="100px"></asp:TextBox>
        </div>
     </div>
    <br />
     <div class="row">
        <div class="col-md-12" style="text-align:center">
            <asp:Button ID="btnSubmit" runat="server" Text="SUBMIT" CssClass="btn btn-default"/> &nbsp; &nbsp;<asp:Button ID="btnReset" runat="server" Text="RESET" CssClass="btn btn-default"/>
       </div> 
    </div>
    <br />
    <div class="row">
        <div class="col-md-12" style="text-align:center">
            <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" Font-Bold="true" ></asp:Label>
        </div> 
    </div> 
</asp:Content>
