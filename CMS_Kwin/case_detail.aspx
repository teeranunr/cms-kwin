﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="case_detail.aspx.vb" Inherits="CMS_Kwin.case_detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <br />
    <div class="row">
        <div class="col-sm-3 col-md-3 label-right">
            <asp:Label ID="Label1" runat="server" Text="MOBILE NO: " Font-Bold="true" ></asp:Label>
        </div> 
        <div class="col-sm-9 col-md-9 label-right" style="text-align:left">
            <asp:TextBox ID="txbMobileNo" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
    </div>
    <br />
    <div class="row">
        <div class="col-xs-12 col-sm-3 col-md-3 label-right">
            <asp:Label ID="Label2" runat="server" Text="START DATE: " Font-Bold="true" ></asp:Label>
        </div> 
        <div class="col-xs-12 col-sm-3 col-md-3" style="padding-right:0px;">
            <asp:TextBox ID="txbStart" name="txbStart" runat="server" CssClass="form-control txbinline"></asp:TextBox>
            <asp:Button ID="btnStart" runat="server" Text="..." CssClass="btn btn-default"/>
            <div id="divCalendar" style="POSITION: absolute; z-index:10; left: 50%; top: 50%; visibility: visible;" class="popupcalendar">
                <asp:Calendar ID="cldStart" runat="server" BackColor="White"></asp:Calendar>                 
            </div> 
        </div>
        <div class="padding-small"></div> 
        <div class="col-xs-12 col-sm-3 col-md-3 label-right">
            <asp:Label ID="Label3" runat="server" Text="END DATE: " Font-Bold="true" ></asp:Label>
        </div> 
        <div class="col-xs-12 col-sm-3 col-md-3" style="text-align:left; padding-right:0px;">
                <asp:TextBox ID="txbEnd" runat="server" CssClass="form-control txbinline"></asp:TextBox>
            <asp:Button ID="btnEnd" runat="server" Text="..." CssClass="btn btn-default"/>
                        <div id="div1" style="POSITION:absolute; z-index:10; left:10%; top:50%; visibility:visible;">
                <asp:Calendar ID="cldEnd" runat="server" BackColor="White"></asp:Calendar>  
            </div>
        </div> 
    </div> 
    <br />
    <div class="row">
        <div class="col-sm-3 col-md-3 label-right">
            <asp:Label ID="Label4" runat="server" Text="STATUS: " Font-Bold="true" ></asp:Label>
        </div> 
        <div class="col-sm-9 col-md-9" style="text-align:left">
            <asp:DropDownList ID="ddlStatus" runat="server">
                <asp:ListItem Text="All" Value=""></asp:ListItem>
                <asp:ListItem Text="Complete" Value="0"></asp:ListItem>
                <asp:ListItem Text="Incomplete" Value="1"></asp:ListItem>
                <asp:ListItem Text="Waiting" Value="2"></asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>
    <br />
    <div class="row">
        <div class="col-md-12" style="text-align:center">
            <asp:Button ID="btnSearch" runat="server" Text="SEARCH" CssClass="btn btn-default"/>
            <asp:Button ID="btnReset" runat="server" Text="RESET" CssClass="btn btn-default"/>
        </div> 
    </div>
    <br />
    <div class="row">
        <div class="col-md-12" style="text-align:center">
            <asp:Label ID="lblerror" runat="server" Text="" Visible="false" Font-Bold="true" ForeColor="Red"></asp:Label>
        </div> 
    </div>
    <br /> 
    <div class="row">
         <asp:GridView ID="GridView1" runat="server" BackColor="White" BorderColor="#999999" 
            BorderStyle="Solid" BorderWidth="1px" CellPadding="3" ForeColor="Black" 
            GridLines="Vertical" AutoGenerateColumns="False"
            PageSize="20" AllowPaging="True" HeaderStyle-Font-Size="10px"
            Width="100%" DataKeyNames="CIQ_ID" >
            <AlternatingRowStyle BackColor="#CCCCCC"/>
            <Columns>
                <asp:TemplateField HeaderText="STATUS" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <asp:Image ID="imgStatus" runat="server" />
                    </ItemTemplate> 
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <div class="row">
                             <div class="col-sm-4 col-md-4">
                                 <asp:Label ID="Label5" runat="server" Text="Date" Font-Bold="true"></asp:Label>
                             </div> 
                             <div class="col-sm-4 col-md-4">
                                  <asp:Label ID="Label6" runat="server" Text="Mobile No" Font-Bold="true"></asp:Label>
                             </div> 
                             <div class="col-sm-4 col-md-4">
                                 <asp:Label ID="Label7" runat="server" Text="Description" Font-Bold="true"></asp:Label>
                             </div> 
                        </div>
                        <div class="row">
                             <div class="col-sm-4 col-md-4">
                                 <asp:Label ID="Label8" runat="Server" Text='<%#Eval("CREATE_DATE")%>' />
                             </div> 
                             <div class="col-sm-4 col-md-4">
                                  <asp:Label ID="phoneNo" runat="Server" Text='<%#Eval("PHONE_NO")%>' />
                             </div> 
                             <div class="col-sm-4 col-md-4">
                                  <asp:Label ID="detail" runat="Server" Text='<%#Eval("CIQ_DETAIL")%>' />
                             </div> 
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <asp:Label ID="Label9" runat="Server" Text="Excel File: " />
                                <asp:FileUpload ID="FileUploadExcel" runat="server" />
                                <asp:Label ID="lblCreateExcel" runat="Server" Text='<%#GetFileName(Eval("EXCEL_FILE_ID"), "create_date")%>' />
                                <asp:Label ID="lblFileNameExcel" runat="Server" Text='<%#GetFileName(Eval("EXCEL_FILE_ID"), "excel")%>' />
                                <asp:Button ID="btnUploadExcel" runat="server" Text="UPLOAD" CommandName="uploadexcel" CommandArgument='<%#Eval("CIQ_ID")%>' CssClass="btn btn-default"/>
                                <asp:Button ID="btnRemoveExcel" runat="server" Text="REMOVE" CommandName="removeexcel" CommandArgument='<%#Eval("CIQ_ID")%>' CssClass="btn btn-default"/> 
                                <asp:Button ID="btnDownloadExcel" runat="server" Text="DOWNLOAD" CommandName="downloadexcel" CommandArgument='<%#Eval("CIQ_ID")%>' CssClass="btn btn-default"/> 
                            </div>
                        </div> 
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <asp:Label ID="Label10" runat="Server" Text="Voice File: " />
                                <asp:FileUpload ID="FileUploadVoice" runat="server" Visible="true" CssClass="upload-file"/>
                                <asp:Label ID="lblCreateVoice" runat="Server" Text='<%#GetFileName(Eval("VOICE_FILE_ID"), "create_date")%>' />
                                <asp:Label ID="lblFileNameVoice" runat="Server" Text='<%#GetFileName(Eval("VOICE_FILE_ID"), "voice")%>' />
                                <asp:Button ID="btnUploadVoice" runat="server" Text="UPLOAD" CommandName="uploadvoice" CommandArgument='<%#Eval("CIQ_ID")%>' CssClass="btn btn-default"/>
                                <asp:Button ID="btnRemoveVoice" runat="server" Text="REMOVE" CommandName="removevoice" CommandArgument='<%#Eval("CIQ_ID")%>' CssClass="btn btn-default"/>
                                <asp:Button ID="btnDownloadVoice" runat="server" Text="DOWNLOAD" CommandName="downloadvoice" CommandArgument='<%#Eval("CIQ_ID")%>' CssClass="btn btn-default"/>
                            </div> 
                        </div>
                    </ItemTemplate> 
                </asp:TemplateField>      
<%--                <asp:TemplateField HeaderText="STATUS">
                     <ItemTemplate>
                         <asp:Image ID="imgStatus" runat="server" />
                     </ItemTemplate> 
                </asp:TemplateField> 
                <asp:TemplateField HeaderText="MOBILE NO">
                     <ItemTemplate>
                         <asp:Label ID="phoneNo" runat="Server" Text='<%#Eval("PHONE_NO")%>' />
                     </ItemTemplate> 
                </asp:TemplateField> 
                <asp:TemplateField HeaderText="DETAIL">
                     <ItemTemplate>
                         <asp:Label ID="detail" runat="Server" Text='<%#Eval("CIQ_DETAIL")%>' />
                     </ItemTemplate> 
                </asp:TemplateField> 
                <asp:TemplateField HeaderText="EXCEL">
                    <ItemTemplate>
                        <div class="row">
                            <div class="col-md-7">
                                <asp:FileUpload ID="FileUploadExcel" runat="server"/>
                                <asp:Label ID="lblCreateExcel" runat="Server" Text='<%#GetFileName(Eval("EXCEL_FILE_ID"), "create_date")%>' /><br />
                                <asp:Label ID="lblFileNameExcel" runat="Server" Text='<%#GetFileName(Eval("EXCEL_FILE_ID"), "excel")%>' />
                            </div>
                            <div class="col-md-5">
                                <asp:Button ID="btnUploadExcel" runat="server" Text="UPLOAD" CommandName="uploadexcel" CommandArgument='<%#Eval("CIQ_ID")%>' CssClass="btn btn-default"/>
                                <asp:Button ID="btnRemoveExcel" runat="server" Text="REMOVE" CommandName="removeexcel" CommandArgument='<%#Eval("CIQ_ID")%>' CssClass="btn btn-default"/> 
                                <asp:Button ID="btnDownloadExcel" runat="server" Text="DOWNLOAD" CommandName="downloadexcel" CommandArgument='<%#Eval("CIQ_ID")%>' CssClass="btn btn-default"/> 
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="VOICE">
                     <ItemTemplate>
                         <div class="row">
                            <div class="col-md-7">
                                <asp:FileUpload ID="FileUploadVoice" runat="server" Visible="true"/>
                                <asp:Label ID="lblCreateVoice" runat="Server" Text='<%#GetFileName(Eval("VOICE_FILE_ID"), "create_date")%>' /><br />
                                <asp:Label ID="lblFileNameVoice" runat="Server" Text='<%#GetFileName(Eval("VOICE_FILE_ID"), "voice")%>' />
                            </div>
                            <div class="col-md-5">
                                <asp:Button ID="btnUploadVoice" runat="server" Text="UPLOAD" CommandName="uploadvoice" CommandArgument='<%#Eval("CIQ_ID")%>' CssClass="btn btn-default"/>
                                <asp:Button ID="btnRemoveVoice" runat="server" Text="REMOVE" CommandName="removevoice" CommandArgument='<%#Eval("CIQ_ID")%>' CssClass="btn btn-default"/>
                                <asp:Button ID="btnDownloadVoice" runat="server" Text="DOWNLOAD" CommandName="downloadvoice" CommandArgument='<%#Eval("CIQ_ID")%>' CssClass="btn btn-default"/>
                            </div> 
                        </div>
                     </ItemTemplate> 
                </asp:TemplateField>--%>
            </Columns>
            <FooterStyle BackColor="#CCCCCC" />
            <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#808080" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#383838" />
        </asp:GridView>
    </div>
    <script>
        $(function () {
            $("#txbStart, #SearchDateEnd").datepicker({
                dateFormat: 'dd/mm/yy', isBuddhist: true, dayNames: ['อาทิตย์', 'จันทร์', 'อังคาร', 'พุธ', 'พฤหัสบดี', 'ศุกร์', 'เสาร์'],
                dayNamesMin: ['อา.', 'จ.', 'อ.', 'พ.', 'พฤ.', 'ศ.', 'ส.'],
                monthNames: ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'],
                monthNamesShort: ['ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.']

            });
        });
    </script>
</asp:Content>

