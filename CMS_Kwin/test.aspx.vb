﻿Public Class test
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim url As String = Request.Url.AbsolutePath
        Response.Write(url)
    End Sub

    Private Sub TextBox1_PreRender(sender As Object, e As EventArgs) Handles TextBox1.PreRender
        Calendar1.Visible = True
    End Sub

    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles TextBox1.TextChanged
        Calendar1.Visible = True
    End Sub

    Private Sub GridView1_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GridView1.RowDataBound
        If e.Row.RowType = DataControlRowType.Header Then
            Dim row As New GridViewRow(0, -1, DataControlRowType.Header, DataControlRowState.Normal)
            Dim left As TableCell = New TableHeaderCell()
            left.ColumnSpan = 6
            row.Cells.Add(left)

            'spanned cell that will span the columns i want to give the additional header
            Dim totals As TableCell = New TableHeaderCell()
            totals.ColumnSpan = GridView1.Columns.Count - 3
            totals.Text = "Additional Header"
            row.Cells.Add(totals)

            'Add the new row to the gridview as the master header row
            'A table is the only Control (index[0]) in a GridView
            DirectCast(GridView1.Controls(0), Table).Rows.AddAt(0, row)
        End If
    End Sub

    Protected Sub GridView1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles GridView1.SelectedIndexChanged

    End Sub
End Class