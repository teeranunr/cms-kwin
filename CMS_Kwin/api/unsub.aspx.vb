﻿Imports System.Data.SqlClient

Public Class unsub1
    Inherits System.Web.UI.Page
    Private objDs As DataSet
    Private funCenter As New funCenter
    Private strSql As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim auth As String
        Dim mobile_no As String
        Dim sv_id As String
        Dim str As New StringBuilder()
        'Dim strSvId As String
        Dim strOptCode As String = ""
        Dim MyConn As SqlConnection
        Dim sqlTrans As SqlTransaction
        Dim strStatus As String = "Unsub Success"
        auth = Request.QueryString("auth")
        mobile_no = Request.QueryString("mobile_no")
        sv_id = Request.QueryString("sv_id")
        Try
            If auth = "adv@m1tt" Then
                'Dim MyConn As SqlConnection
                MyConn = funCenter.Connection()
                If MyConn.State = ConnectionState.Closed Then
                    MyConn.Open()
                End If
                mobile_no = "66" & Right(mobile_no, 9)
                strSql = "SELECT OPT_CODE FROM SUBSCRIPTION WHERE PHONE_NO = @mobile_no AND SV_ID = @sv_id"
                Dim command As New SqlCommand()
                command.Connection = MyConn
                command.CommandText = strSql
                command.Parameters.Add(New SqlParameter("@mobile_no", mobile_no))
                command.Parameters.Add(New SqlParameter("@sv_id", sv_id))
                Dim reader As SqlDataReader = command.ExecuteReader()
                If reader.Read() Then
                    strOptCode = reader.GetString(0)
                Else

                End If
                If MyConn.State = ConnectionState.Open Then
                    MyConn.Close()
                End If

                MyConn = funCenter.Connection()
                MyConn.Open()
                sqlTrans = MyConn.BeginTransaction
                Dim usp_ReqService As New SqlCommand()
                usp_ReqService.Connection = MyConn
                usp_ReqService.Transaction = sqlTrans

                strSql = "EXEC [VTRAN_3].[dbo].[usp_Backoffice_ReqTrans] 'unsub',@mobile_no, @opt_code,@sv_id"

                usp_ReqService.CommandText = strSql
                usp_ReqService.Parameters.Add(New SqlParameter("@mobile_no", mobile_no))
                usp_ReqService.Parameters.Add(New SqlParameter("@opt_code", strOptCode))
                usp_ReqService.Parameters.Add(New SqlParameter("@sv_id", sv_id))
                usp_ReqService.ExecuteNonQuery()
                sqlTrans.Commit()
                MyConn.Close()
            Else
                strStatus = "Unsub Fail"
            End If
        Catch ex As Exception
            strStatus = ex.Message
        End Try
        Response.Write(strStatus)
        'Dim webClient As New System.Net.WebClient
        'Dim result As String = webClient.DownloadString("http://202.43.45.148/sms/callcenter_sub.ashx?msisdn=" & strMobileNo & "&action=unsub&sv=" & strServiceCode)
    End Sub

End Class