﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Data.SqlClient
Imports Newtonsoft.Json

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class customer
    Inherits System.Web.Services.WebService
    Private strSql As String
    Private strMobileNo As String
    Private funCenter As New funCenter

    <WebMethod()> _
    Public Function HelloWorld() As String
        Return "Hello World"
    End Function


    <WebMethod()> _
    Public Function Subscription(mobileNo As String) As String
        Dim MyConn As SqlConnection
        MyConn = funCenter.Connection()
        If MyConn.State = ConnectionState.Closed Then
            MyConn.Open()
        End If
        Dim ds As New DataSet
        Dim dt As DataTable
        strMobileNo = "66" & Right(mobileNo, 9)
        strSql = "SELECT TOP 10 B.OPT_CODE,B.SUB_ID,B.SUBL_ID,B.SV_ID,S.SV_NAME," & _
                 " B.STATUS,ISNULL(CONVERT(varchar, B.CREATE_DATE, 120), '') AS CREATE_DATE," & _
                 " ISNULL(CONVERT(varchar, B.END_DATE, 120), '') AS END_DATE," & _
                 " ISNULL(CONVERT(varchar, B.CANCEL_DATE, 120), '') AS CANCEL_DATE," & _
                 " ISNULL(B.REMARK,'') AS REMARK,ISNULL(B.SUB_CHANNEL,'') AS SUB_CHANNEL," & _
                 " ISNULL(B.CANCEL_CHANNEL,'') AS CANCEL_CHANNEL,ISNULL(B.OPT_LINKED_ID,'') AS OPT_LINKED_ID" & _
                 " FROM SUBSCRIPTION B,SERVICE S" & _
                 " WHERE S.SV_ID = B.SV_ID" & _
                 " AND B.PHONE_NO = @mobileNo" & _
                 " ORDER BY B.STATUS ASC,B.CREATE_DATE DESC"
        Dim command As New SqlCommand()
        command.Connection = MyConn
        command.CommandText = strSql
        command.Parameters.Add(New SqlParameter("@mobileNo", mobileNo))
        Dim dtAdapter As New SqlDataAdapter
        dtAdapter.SelectCommand = command
        dtAdapter.Fill(ds)
        dt = ds.Tables(0)
        dtAdapter = Nothing
        MyConn.Close()
        MyConn = Nothing
        'Dim reader As SqlDataReader = command.ExecuteReader()
        'If reader.HasRows Then
        '    While reader.Read()
        '        Dim value1 = reader.GetValue(0)

        '        Dim value2 = reader.GetValue(1)

        '        Dim value3 = reader.GetValue(2)
        '    End While
        'End If

        'If reader.Read() Then

        'End If
        Dim json = JsonConvert.SerializeObject(dt, Formatting.Indented)
        Return json
        'Return "subscription"
    End Function
End Class