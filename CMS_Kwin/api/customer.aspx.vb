﻿Imports System.Data.SqlClient
Imports Newtonsoft.Json

Public Class customer1
    Inherits System.Web.UI.Page
    Dim resp As String
    Private strSql As String
    Private strMobileNo As String
    Private funCenter As New funCenter
    Private objDs As DataSet
    Private MyConn As SqlConnection

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim mobileNo As String
            mobileNo = Request.QueryString("mobile_no")
            MyConn = funCenter.Connection()
            If MyConn.State = ConnectionState.Closed Then
                MyConn.Open()
            End If
            Dim ds As New DataSet
            Dim dt As DataTable
            strMobileNo = "66" & Right(mobileNo, 9)
            strSql = "SELECT B.OPT_CODE,B.SV_ID,S.SV_NUMBER + ' - ' + S.SV_NAME AS SV_NAME," &
                     " B.STATUS,ISNULL(CONVERT(varchar, B.CREATE_DATE, 120), '') AS CREATE_DATE," &
                     " ISNULL(CONVERT(varchar, B.END_DATE, 120), '') AS END_DATE," &
                     " ISNULL(CONVERT(varchar, B.CANCEL_DATE, 120), '') AS CANCEL_DATE," &
                     " ISNULL(B.REMARK,'') AS REMARK,ISNULL(B.SUB_CHANNEL,'') AS SUB_CHANNEL," &
                     " ISNULL(B.CANCEL_CHANNEL,'') AS CANCEL_CHANNEL" &
                     " FROM SUBSCRIPTION B,SERVICE S" &
                     " WHERE S.SV_ID = B.SV_ID" &
                     " AND B.PHONE_NO = @mobileNo" &
                     " ORDER BY B.STATUS ASC,B.CREATE_DATE DESC"
            Dim command As New SqlCommand()
            command.Connection = MyConn
            command.CommandText = strSql
            command.Parameters.Add(New SqlParameter("@mobileNo", strMobileNo))
            Dim dtAdapter As New SqlDataAdapter
            dtAdapter.SelectCommand = command
            dtAdapter.Fill(ds)
            dt = ds.Tables(0)
            dtAdapter = Nothing
            MyConn.Close()
            MyConn = Nothing

            'resp = JsonConvert.SerializeObject(dt, Formatting.Indented)
            Dim SubscriptionResponse As New SubscriptionResponse()
            SubscriptionResponse.status = "S"
            SubscriptionResponse.desc = "Success"
            SubscriptionResponse.Subscription = dt
            'Dim mainResponse As New MainResponse()
            'mainResponse.status = "S"
            'mainResponse.desc = "Success"

            Dim json As String = JsonConvert.SerializeObject(SubscriptionResponse)

            'resp = JsonConvert.SerializeObject(dt, Formatting.Indented)
            Response.Write(json)
        Catch ex As Exception
            Response.Write(ex.ToString())
        End Try
    End Sub

End Class