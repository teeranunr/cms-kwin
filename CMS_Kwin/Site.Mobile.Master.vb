﻿Public Class Site_Mobile
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim Cookie As HttpCookie
        Dim strUsrId As String
        Dim intUserId As Int16
        Try
            Cookie = Request.Cookies("USR")
            strUsrId = Cookie.Item("USR_ID")
            If (Request.Cookies("USR") IsNot Nothing) Then
                If (Request.Cookies("USR")("USR_ID") IsNot Nothing) Then
                    btnLogout.Visible = True
                    intUserId = Request.Cookies("USR")("USR_ID")
                    If intUserId = 1 Or intUserId = 3 Then
                        import.Visible = True
                        lblHeader.Text = "CONTENT:"
                    ElseIf intUserId = 2 Then
                        lblHeader.Text = "Report:"
                        likSubscription.Visible = False
                        likTransaction.Visible = True
                        likTransactionPartner.Visible = True
                        likTransaction4194006.Visible = True
                        likTransService.Visible = False
                    ElseIf intUserId = 5 Then
                        lblHeader.Text = "CUSTOMER INFORMATION"
                    End If
                Else
                    lblHeader.Text = "CONTENT MANAGEMENT SYSTEM"
                    btnLogout.Visible = False
                    import.Visible = False
                    intUserId = 0
                End If
            Else
            End If
        Catch ex As Exception
            btnLogout.Visible = False
            import.Visible = False
        Finally

        End Try
        Dim url As String = Request.Url.AbsolutePath
        If intUserId = 0 Then
            If url = "/Account/login" Then

            Else
                Response.Redirect("/Account/login.aspx")
            End If
        End If
    End Sub

End Class