﻿Imports System.Globalization
Imports System.IO

Public Class revenue
    Inherits System.Web.UI.Page
    Private strSql As String
    Private funCenter As New funCenter
    Private objDs As DataSet
    Private strType As String
    Private strStartDate As String
    Private strEndDate As String
    Private strStartTime As String
    Private strStopTime As String
    Private strDate As String
    Private strSvId As String
    Dim total1 As Int32 = 0
    Dim total2 As Int32 = 0
    Dim total3 As Int32 = 0
    Dim total4 As Int32 = 0
    Dim total5 As Int32 = 0
    Dim total6 As Int32 = 0
    Dim total7 As Int32 = 0
    Dim total8 As Int32 = 0
    Dim total9 As Int32 = 0
    Dim total10 As Int32 = 0
    Dim total11 As Int32 = 0
    Dim total12 As Int32 = 0
    Dim total13 As Int32 = 0
    Private strCultureInfo As CultureInfo = New CultureInfo("en-US")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        strType = Request.QueryString("type")
        strSvId = Request.QueryString("sv_id")
        If Not IsPostBack() Then
            strDate = Request.QueryString("date")
            cldStart.Visible = False
            cldEnd.Visible = False
            Dim dateNow As DateTime = DateTime.Now.AddDays(-1)
            txbStart.Text = dateNow.Year & Right("0" & dateNow.Month, 2) & Right("0" & dateNow.Day, 2)
            txbEnd.Text = dateNow.Year & Right("0" & dateNow.Month, 2) & Right("0" & dateNow.Day, 2)
            If strType = "4" Then
                lblTime.Visible = True
                ddlStartTime.Visible = True
                ddlStopTime.Visible = True
                ddlService.Visible = True
                div_diff.Visible = True
                div_diff2.Visible = True
                div_diff3.Visible = True
            Else
                lblService.Visible = False
                lblTime.Visible = False
                ddlStartTime.Visible = False
                ddlStopTime.Visible = False
                ddlService.Visible = False
                div_diff.Visible = False
                div_diff2.Visible = False
                div_diff3.Visible = False
            End If
            strSql = "SELECT SV_ID,SV_NAME" & _
                    " FROM SERVICE" & _
                    " WHERE SV_ID in(1,2,3,4,5,6,7,8)"
            ddlService.DataSource = funCenter.Data_dropdown(strSql, "SV_NAME", "SV_ID", "", "", " ALL ")
            ddlService.DataTextField = "key"
            ddlService.DataValueField = "value"
            ddlService.DataBind()
            If strSvId <> "" Then
                ddlService.SelectedValue = strSvId
            End If
            BindData()
        End If
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        If strType = "4" Then
            strDate = ""
        Else
        End If
        BindData()
    End Sub

    Sub BindData()
        cldStart.Visible = False
        cldEnd.Visible = False
        If txbStart.Text.Trim() <> "" And txbEnd.Text.Trim() <> "" Then
            If strType = "1" Then
                'strSql = "SELECT YYYYMMDD,SV_ID,SUBS_NUM,UNSUBS_NUM,ACTIVE_NUM" & _
                '         " FROM REP_SUBSCRIPTION" & _
                '         " WHERE YYYYMMDD BETWEEN '" & txbStart.Text.Trim() & "' AND '" & txbEnd.Text.Trim() & "'" & _
                '         " ORDER BY YYYYMMDD,SV_ID"
                strSql = "SELECT A.YYYYMMDD,A.SV_ID,A.SUBS_NUM,A.UNSUBS_NUM,A.ACTIVE_NUM,B.TOTAL_ACTIVE " & _
                        " FROM" & _
                        " (SELECT YYYYMMDD,SV_ID,SUBS_NUM,UNSUBS_NUM,ACTIVE_NUM" & _
                        " FROM REP_SUBSCRIPTION" & _
                        " WHERE YYYYMMDD BETWEEN '" & txbStart.Text.Trim() & "' AND '" & txbEnd.Text.Trim() & "') AS A" & _
                        " Left Join" & _
                        " (SELECT YYYYMMDD,SUM(ACTIVE_NUM) AS TOTAL_ACTIVE" & _
                        " FROM REP_SUBSCRIPTION" & _
                        " WHERE YYYYMMDD BETWEEN '" & txbStart.Text.Trim() & "' AND '" & txbEnd.Text.Trim() & "'" & _
                        " GROUP BY YYYYMMDD) AS B" & _
                        " ON A.YYYYMMDD = B.YYYYMMDD" & _
                        " ORDER BY A.YYYYMMDD,A.SV_ID"
                objDs = funCenter.QueryData(strSql, "DATA", "", "", "", "report")
                If objDs.Tables("DATA").Rows.Count > 0 Then
                    GridView1.DataSource = objDs.Tables("DATA")
                    GridView1.DataBind()
                    'For Each rowItem As GridViewRow In GridView1.Rows
                    '    rowItem.Attributes.Add("onmouseover", "javascript:this.className = 'rowStyle'")
                    '    rowItem.Attributes.Add("onmouseout", "javascript:this.className = ''")
                    'Next
                    GridView1.Visible = True
                    lblError.Visible = False
                    btnExportExcel.Visible = True
                Else
                    GridView1.DataSource = Nothing
                    GridView1.Visible = False
                    lblError.Text = "No Data!"
                    lblError.Visible = True
                    btnExportExcel.Visible = False
                End If
            ElseIf strType = "2" Then
                strSql = "SELECT TOP 100 Q.YYYYMMDD,SUM(ISNULL(Q.TOTAL_TRANS,0)) AS TOTAL_TRANS," & _
                        " SUM(ISNULL(Q.TOTAL_SENT_TRANS,0)) AS TOTAL_SENT_TRANS, SUM(ISNULL(Q.SUCCESS_TRANS,0)) AS SUCCESS_TRANS," & _
                        " SUM(ISNULL(Q.TOTAL_AMOUNT,0)) AS TOTAL_AMOUNT," & _
                        " Format(CONVERT(FLOAT,SUM(ISNULL(Q.success_trans,0))) * 100 / SUM(ISNULL(Q.TOTAL_TRANS,0)),'##.##') as SUCCESS_RATE,N.NOTE" & _
                        " FROM REP_TRANSACTION AS Q " & _
                        " LEFT OUTER JOIN REP_NOTE AS N" & _
                        " ON Q.YYYYMMDD = N.YYYYMMDD" & _
                        " WHERE Q.YYYYMMDD BETWEEN '" & txbStart.Text.Trim() & "' AND '" & txbEnd.Text.Trim() & "'" & _
                        " GROUP BY Q.YYYYMMDD,N.NOTE" & _
                        " ORDER BY Q.YYYYMMDD"
                objDs = funCenter.QueryData(strSql, "DATA", "", "", "", "report")
                If objDs.Tables("DATA").Rows.Count > 0 Then
                    'Response.Write(objDs.Tables("DATA").Rows(0).Item("SUCCESS_RATE"))
                    GridView2.DataSource = objDs.Tables("DATA")
                    GridView2.DataBind()
                    GridView2.Visible = True
                    lblError.Visible = False
                    btnExportExcel.Visible = True
                Else
                    GridView2.DataSource = Nothing
                    GridView2.Visible = False
                    lblError.Text = "No Data!"
                    lblError.Visible = True
                    btnExportExcel.Visible = False
                End If
            ElseIf strType = "3" Then
                strSql = "SELECT YYYYMMDD,S.SV_ID,S.SV_NAME,T.SEQ_NO," & _
                          " Case CNT_TYPE" & _
                          " WHEN 'A'" & _
                          " THEN 'A:เฉลยวันก่อนหน้า'" & _
                          " WHEN 'Q'" & _
                          " THEN 'Q:คำถามประจำวัน'" & _
                          " WHEN 'C'" & _
                          " THEN 'C:ตัวเลือกของคำถามประจำวัน'" & _
                          " Else ''" & _
                          " End" & _
                          " AS CNT_TYPE ,SUM(ISNULL(T.TOTAL_TRANS,0)) AS TOTAL_TRANS," & _
                          " SUM(ISNULL(T.TOTAL_SENT_TRANS,0)) AS TOTAL_SENT_TRANS," & _
                          " SUM(ISNULL(T.SUCCESS_TRANS,0)) AS SUCCESS_TRANS,SUM(ISNULL(T.TOTAL_AMOUNT,0)) AS TOTAL_AMOUNT" & _
                          " FROM REP_TRANSACTION T,VCONT.dbo.SERVICE S" & _
                          " WHERE YYYYMMDD BETWEEN '" & txbStart.Text.Trim() & "' AND '" & txbEnd.Text.Trim() & "'" & _
                          " AND T.SV_ID = S.SV_ID" & _
                          " GROUP BY YYYYMMDD,CNT_TYPE,S.SV_NAME,S.SV_ID,T.TOTAL_TRANS,T.TOTAL_SENT_TRANS,T.TOTAL_AMOUNT,T.SEQ_NO" & _
                          " ORDER BY YYYYMMDD,S.SV_ID,T.SEQ_NO"
                objDs = funCenter.QueryData(strSql, "DATA", "", "", "", "report")
                If objDs.Tables("DATA").Rows.Count > 0 Then
                    GridView3.DataSource = objDs.Tables("DATA")
                    GridView3.Visible = True
                    GridView3.DataBind()
                    lblError.Visible = False
                    btnExportExcel.Visible = True
                Else
                    GridView3.DataSource = Nothing
                    GridView3.Visible = False
                    lblError.Text = "No Data!"
                    lblError.Visible = True
                    btnExportExcel.Visible = False
                End If
            Else

                'If strSvId <> "" Then
                If strDate <> "" Then
                    txbStart.Text = strDate
                    txbEnd.Text = strDate
                Else

                End If
                lblTime.Visible = True
                ddlStartTime.Visible = True
                ddlStopTime.Visible = True
                ddlService.Visible = True
                strStartTime = ddlStartTime.SelectedValue()
                strStopTime = ddlStopTime.SelectedValue()
                strStartTime = "00:00"
                strStopTime = "23:00"
                'If strDate <> "" Then
                '    strStartDate = strDate
                '    strEndDate = strDate
                'Else
                '    strEndDate = txbEnd.Text.Trim()
                '    strStartDate = txbStart.Text.Trim()
                'End If
                Dim strSvIdSelect As String = "1"
                strSvIdSelect = ddlService.SelectedValue()
                If strSvIdSelect = "0" Then
                    strSvIdSelect = "1,2,3,4,5,6,7"
                    ddlService.SelectedValue = "0"
                End If
                strStartTime = ddlStartTime.SelectedValue()
                strStopTime = ddlStopTime.SelectedValue()
                strSql = "SELECT DISTINCT R.ROUND_NO,R.YYYYMMDD," & _
                        " ISNULL(A.SENT_TIME,'')AS A_SENT_TIME,ISNULL(A.SENT_TRANS,'') AS A_SENT_TRANS," & _
                        " ISNULL(A.DR_TRANS,'') AS A_DR_TRANS,ISNULL(A.SUCCESS_TRANS,'') AS A_SUCCESS_TRANS," & _
                        " ISNULL(C.SENT_TIME,'')AS C_SENT_TIME,ISNULL(C.SENT_TRANS,'') AS C_SENT_TRANS," & _
                        " ISNULL(C.DR_TRANS,'') AS C_DR_TRANS,ISNULL(C.SUCCESS_TRANS,'') AS C_SUCCESS_TRANS," & _
                        " ISNULL(Q.SENT_TIME,'')AS Q_SENT_TIME,ISNULL(Q.SENT_TRANS,'') AS Q_SENT_TRANS," & _
                        " ISNULL(Q.DR_TRANS,'') AS Q_DR_TRANS,ISNULL(Q.SUCCESS_TRANS,'') AS Q_SUCCESS_TRANS" & _
                        " FROM" & _
                        " (SELECT DISTINCT ROUND_NO,YYYYMMDD" & _
                        " FROM REP_TRANSACTION_ROUND" & _
                        " WHERE YYYYMMDD BETWEEN '" & txbStart.Text.Trim() & "' AND '" & txbEnd.Text.Trim() & "'" & _
                        " AND SENT_TIME BETWEEN '" & strStartTime & "' AND '" & strStopTime & "'" & _
                        " AND SV_ID IN(" & strSvIdSelect & ")) AS R" & _
                        " Left Join" & _
                        " (SELECT DISTINCT YYYYMMDD,ROUND_NO,SENT_TIME,SENT_TRANS,DR_TRANS,SUCCESS_TRANS" & _
                        " FROM REP_TRANSACTION_ROUND" & _
                        " WHERE YYYYMMDD BETWEEN '" & txbStart.Text.Trim() & "' AND '" & txbEnd.Text.Trim() & "'" & _
                        " AND SV_ID IN(" & strSvIdSelect & ")" & _
                        " AND SENT_TIME BETWEEN '" & strStartTime & "' AND '" & strStopTime & "'" & _
                        " AND CNT_TYPE = 'A') AS A" & _
                        " ON R.ROUND_NO = A.ROUND_NO AND R.YYYYMMDD = A.YYYYMMDD" & _
                        " Left Join" & _
                        " (SELECT DISTINCT YYYYMMDD,ROUND_NO,SENT_TIME,SENT_TRANS,DR_TRANS,SUCCESS_TRANS" & _
                        " FROM REP_TRANSACTION_ROUND" & _
                        " WHERE YYYYMMDD BETWEEN '" & txbStart.Text.Trim() & "' AND '" & txbEnd.Text.Trim() & "'" & _
                        " AND SV_ID IN(" & strSvIdSelect & ")" & _
                        " AND SENT_TIME BETWEEN '" & strStartTime & "' AND '" & strStopTime & "'" & _
                        " AND CNT_TYPE = 'C') AS C" & _
                        " ON R.ROUND_NO = C.ROUND_NO  AND R.YYYYMMDD =C.YYYYMMDD" & _
                        " Left Join" & _
                        " (SELECT DISTINCT YYYYMMDD,ROUND_NO,SENT_TIME,SENT_TRANS,DR_TRANS,SUCCESS_TRANS" & _
                        " FROM REP_TRANSACTION_ROUND" & _
                        " WHERE YYYYMMDD BETWEEN '" & txbStart.Text.Trim() & "' AND '" & txbEnd.Text.Trim() & "'" & _
                        " AND SV_ID IN(" & strSvIdSelect & ")" & _
                        " AND SENT_TIME BETWEEN '" & strStartTime & "' AND '" & strStopTime & "'" & _
                        " AND CNT_TYPE = 'Q') AS Q" & _
                        " ON R.ROUND_NO = Q.ROUND_NO  AND R.YYYYMMDD = Q.YYYYMMDD" & _
                        " ORDER BY R.YYYYMMDD,R.ROUND_NO"
                'Response.Write(strSql)
                objDs = funCenter.QueryData(strSql, "DATA", "", "", "", "report")
                If objDs.Tables("DATA").Rows.Count > 0 Then
                    GridView4.DataSource = objDs.Tables("DATA")
                    GridView4.Visible = True
                    GridView4.DataBind()
                    lblError.Visible = False
                    btnExportExcel.Visible = True
                Else
                    GridView4.DataSource = Nothing
                    GridView4.Visible = False
                    lblError.Text = "No Data!"
                    lblError.Visible = True
                    btnExportExcel.Visible = False
                End If
                strDate = ""
            End If
        Else

        End If
    End Sub

    Protected Sub btnStart_Click(sender As Object, e As EventArgs) Handles btnStart.Click
        cldStart.Visible = True
    End Sub

    Protected Sub btnEnd_Click(sender As Object, e As EventArgs) Handles btnEnd.Click
        cldEnd.Visible = True
    End Sub

    Protected Sub cldStart_SelectionChanged(sender As Object, e As EventArgs) Handles cldStart.SelectionChanged
        cldStart.Visible = False
        cldEnd.Visible = False
        txbStart.Text = cldStart.SelectedDate.ToString("yyyyMMdd", strCultureInfo)
        BindData()
    End Sub

    Protected Sub cldEnd_SelectionChanged(sender As Object, e As EventArgs) Handles cldEnd.SelectionChanged
        cldStart.Visible = False
        cldEnd.Visible = False
        txbEnd.Text = cldEnd.SelectedDate.ToString("yyyyMMdd", strCultureInfo)
        BindData()
    End Sub

    Private Sub txbStart_TextChanged(sender As Object, e As EventArgs) Handles txbStart.TextChanged
        cldStart.Visible = True
    End Sub

    Protected Sub btnExportExcel_Click(sender As Object, e As EventArgs) Handles btnExportExcel.Click
        Dim sw As New StringWriter()
        Dim htw As New HtmlTextWriter(sw)
        Dim frm As New HtmlForm()
        Dim strFileName As String
        Dim strDate As String
        Dim strMonth As String
        Dim strYear As String
        If (Len(DateTime.Now.Day.ToString) = 1) Then
            strDate = "0" & DateTime.Now.Day.ToString
        Else
            strDate = DateTime.Now.Day.ToString
        End If
        If (Len(DateTime.Now.Month.ToString) = 1) Then
            strMonth = "0" & DateTime.Now.Month.ToString
        Else
            strMonth = DateTime.Now.Month.ToString
        End If
        strYear = DateTime.Now.Year.ToString

        strFileName = strYear & strMonth & strDate & Right("0" & DateTime.Now.Hour.ToString, 2) & Right("0" & DateTime.Now.Minute.ToString, 2) & Right("0" & DateTime.Now.Second.ToString, 2)
        Response.Clear()
        Response.ClearHeaders()
        Response.ClearContent()
        Response.AddHeader("content-disposition", "attachment;filename=" & strFileName & ".xls")
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.ContentEncoding = Encoding.UTF8
        Response.Charset = Encoding.UTF8.EncodingName
        Response.ContentType = "application/vnd.ms-excel"
        If strType = "1" Then
            frm.Attributes("runat") = "server"
            frm.Controls.Add(GridView1)
            GridView1.HeaderStyle.BackColor = Drawing.Color.White
            GridView1.HeaderStyle.ForeColor = Drawing.Color.Black
            GridView1.AlternatingRowStyle.BackColor = Drawing.Color.White
            GridView1.BorderColor = Drawing.Color.Black
            GridView1.BorderStyle = BorderStyle.Solid
            GridView1.FooterStyle.BackColor = Drawing.Color.White
            GridView1.FooterStyle.ForeColor = Drawing.Color.Black
            GridView1.RenderControl(htw)
        ElseIf strType = "2" Then
            frm.Controls.Add(GridView2)
            GridView2.HeaderStyle.BackColor = Drawing.Color.White
            GridView2.HeaderStyle.ForeColor = Drawing.Color.Black
            GridView2.AlternatingRowStyle.BackColor = Drawing.Color.White
            GridView2.BorderColor = Drawing.Color.Black
            GridView2.BorderStyle = BorderStyle.Solid
            GridView2.FooterStyle.BackColor = Drawing.Color.White
            GridView2.FooterStyle.ForeColor = Drawing.Color.Black
            GridView2.RenderControl(htw)
        ElseIf strType = "3" Then
            frm.Attributes("runat") = "server"
            frm.Controls.Add(GridView3)
            GridView3.HeaderStyle.BackColor = Drawing.Color.White
            GridView3.HeaderStyle.ForeColor = Drawing.Color.Black
            GridView3.AlternatingRowStyle.BackColor = Drawing.Color.White
            GridView3.BorderColor = Drawing.Color.Black
            GridView3.BorderStyle = BorderStyle.Solid
            GridView3.FooterStyle.BackColor = Drawing.Color.White
            GridView3.FooterStyle.ForeColor = Drawing.Color.Black
            GridView3.RenderControl(htw)
        Else
            frm.Attributes("runat") = "server"
            frm.Controls.Add(GridView4)
            GridView4.HeaderStyle.BackColor = Drawing.Color.White
            GridView4.HeaderStyle.ForeColor = Drawing.Color.Black
            GridView4.AlternatingRowStyle.BackColor = Drawing.Color.White
            GridView4.BorderColor = Drawing.Color.Black
            GridView4.BorderStyle = BorderStyle.Solid
            GridView4.FooterStyle.BackColor = Drawing.Color.White
            GridView4.FooterStyle.ForeColor = Drawing.Color.Black
            GridView4.RenderControl(htw)
        End If
        Response.Write(sw.ToString())
        Response.End()
    End Sub

    Function GetServiceName(intSvId As Int16) As String
        Return funCenter.GetServiceName(intSvId)
    End Function

    Protected Sub GridView31_DataBound(sender As Object, e As EventArgs)
        Dim i As Integer = 1
        Dim lnk As HyperLink = DirectCast(GridView3.FindControl("hlnkView"), HyperLink)
        For rowIndex As Integer = GridView3.Rows.Count - 2 To 0 Step -1
            Dim gvRow As GridViewRow = GridView3.Rows(rowIndex)
            Dim gvPreviousRow As GridViewRow = GridView3.Rows(rowIndex + 1)
            For cellCount As Integer = 0 To gvRow.Cells.Count - 1
                If cellCount = 1 Then
                    If i Mod 3 <> 0 Then
                        If gvPreviousRow.Cells(cellCount).RowSpan < 2 Then
                            gvRow.Cells(cellCount).RowSpan = 2
                        Else
                            gvRow.Cells(cellCount).RowSpan = gvPreviousRow.Cells(cellCount).RowSpan + 1
                        End If
                        gvPreviousRow.Cells(cellCount).Visible = False
                    End If
                    i += 1
                Else
                    If gvRow.Cells(cellCount).Text = gvPreviousRow.Cells(cellCount).Text Then
                        If gvPreviousRow.Cells(cellCount).RowSpan < 2 Then
                            gvRow.Cells(cellCount).RowSpan = 2
                        Else
                            gvRow.Cells(cellCount).RowSpan = gvPreviousRow.Cells(cellCount).RowSpan + 1
                        End If
                        gvPreviousRow.Cells(cellCount).Visible = False
                    End If
                End If
            Next
        Next
    End Sub

    Protected Sub GridView1_DataBound(sender As Object, e As EventArgs)
        Dim i As Integer = 1
        'Dim lnk As HyperLink = DirectCast(GridView1.FindControl("hlnkView"), HyperLink)
        For rowIndex As Integer = GridView1.Rows.Count - 2 To 0 Step -1
            Dim gvRow As GridViewRow = GridView1.Rows(rowIndex)
            Dim gvPreviousRow As GridViewRow = GridView1.Rows(rowIndex + 1)
            For cellCount As Integer = 0 To gvRow.Cells.Count - 1
                If cellCount = 0 Or cellCount = 5 Then
                    '    'If i Mod 3 <> 0 Then
                    '    If gvPreviousRow.Cells(cellCount).RowSpan < 2 Then
                    '        gvRow.Cells(cellCount).RowSpan = 2
                    '    Else
                    '        gvRow.Cells(cellCount).RowSpan = gvPreviousRow.Cells(cellCount).RowSpan + 1
                    '    End If
                    '    gvPreviousRow.Cells(cellCount).Visible = False
                    '    'End If
                    '    i += 1
                    'Else
                    If gvRow.Cells(cellCount).Text = gvPreviousRow.Cells(cellCount).Text Then
                        If gvPreviousRow.Cells(cellCount).RowSpan < 2 Then
                            gvRow.Cells(cellCount).RowSpan = 2
                        Else
                            gvRow.Cells(cellCount).RowSpan = gvPreviousRow.Cells(cellCount).RowSpan + 1
                        End If
                        gvPreviousRow.Cells(cellCount).Visible = False
                    End If
                End If
            Next
        Next
    End Sub

    Function GetContentTypeName(strType As String) As String
        If strType = "A" Then
            Return "A:เฉลยวันก่อนหน้า"
        ElseIf strType = "Q" Then
            Return "Q:คำถามประจำวัน"
        ElseIf strType = "C" Then
            Return "C:ตัวเลือกของคำถามประจำวัน"
        Else
            Return ""
        End If
    End Function

    Function GetLink(svid As String, YYYYMMDD As String)
        Return "revenue.aspx?type=4&sv_id=" & svid & "&date=" & YYYYMMDD
    End Function

    'Private Sub GridView4_DataBound(sender As Object, e As EventArgs) Handles GridView4.DataBound
    '    Dim i As Integer = 1
    '    For rowIndex As Integer = GridView4.Rows.Count - 2 To 0 Step -1
    '        Dim gvRow As GridViewRow = GridView4.Rows(rowIndex)
    '        Dim gvPreviousRow As GridViewRow = GridView4.Rows(rowIndex + 1)
    '        For cellCount As Integer = 0 To gvRow.Cells.Count - 1
    '            If cellCount = 0 Then
    '                If gvRow.Cells(cellCount).Text = gvPreviousRow.Cells(cellCount).Text Then
    '                    If gvPreviousRow.Cells(cellCount).RowSpan < 2 Then
    '                        gvRow.Cells(cellCount).RowSpan = 2
    '                    Else
    '                        gvRow.Cells(cellCount).RowSpan = gvPreviousRow.Cells(cellCount).RowSpan + 1
    '                    End If
    '                    gvPreviousRow.Cells(cellCount).Visible = False
    '                End If
    '            Else

    '            End If
    '        Next
    '    Next
    'End Sub

    Private Sub GridView4_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GridView4.RowDataBound
        Dim gvr As GridViewRow = e.Row
        If gvr.RowType = DataControlRowType.Header Then
            Dim row As New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal)

            Dim cell As New TableCell()
            cell.ColumnSpan = 2
            cell.HorizontalAlign = HorizontalAlign.Center
            cell.Text = ""
            row.Cells.Add(cell)

            cell = New TableCell()
            cell.ColumnSpan = 4
            cell.HorizontalAlign = HorizontalAlign.Center
            cell.Text = "A : เฉลย วันก่อนหน้า"
            row.Cells.Add(cell)
            cell = New TableCell()
            cell.ColumnSpan = 4
            cell.HorizontalAlign = HorizontalAlign.Center
            cell.Text = "Q : คำถาม ประจำวัน"
            row.Cells.Add(cell)
            cell = New TableCell()
            cell.ColumnSpan = 4
            cell.HorizontalAlign = HorizontalAlign.Center
            cell.Text = "C : ตัวเลือก ของคำถามประจำวัน"
            row.Cells.Add(cell)
            GridView4.Controls(0).Controls.AddAt(0, row)
        ElseIf gvr.RowType = DataControlRowType.DataRow Then
            total3 += Replace(e.Row.Cells(3).Text, ",", "")
            total4 += Replace(e.Row.Cells(4).Text, ",", "")
            total5 += Replace(e.Row.Cells(5).Text, ",", "")
            total7 += Replace(e.Row.Cells(7).Text, ",", "")
            total8 += Replace(e.Row.Cells(8).Text, ",", "")
            total9 += Replace(e.Row.Cells(9).Text, ",", "")
            total11 += Replace(e.Row.Cells(11).Text, ",", "")
            total12 += Replace(e.Row.Cells(12).Text, ",", "")
            total13 += Replace(e.Row.Cells(13).Text, ",", "")
        ElseIf gvr.RowType = DataControlRowType.Footer Then
            e.Row.Cells(1).Text = "Summary"
            e.Row.Cells(3).Text = formatNumber(total3)
            e.Row.Cells(4).Text = formatNumber(total4)
            e.Row.Cells(5).Text = formatNumber(total5)
            e.Row.Cells(7).Text = formatNumber(total7)
            e.Row.Cells(8).Text = formatNumber(total8)
            e.Row.Cells(9).Text = formatNumber(total9)
            e.Row.Cells(11).Text = formatNumber(total11)
            e.Row.Cells(12).Text = formatNumber(total12)
            e.Row.Cells(13).Text = formatNumber(total13)
            lblSummary.Visible = True
            lblSentTransaction.Text = "Sent - Transaction " & formatNumber(total3 + total7 + total11)
            lblSentTransaction.Visible = True
            lblDRTransaction.Text = "DR - Transaction " & formatNumber(total4 + total8 + total12)
            lblDRTransaction.Visible = True
            lblDRTransactionSuccess.Text = "DR - Transaction Success " & formatNumber(total5 + total9 + total13)
            lblDRTransactionSuccess.Visible = True

            lblDiffTransA.Text = "Diff Trans Sent/DR " & formatNumber(total3 - total4)
            lblDiffTransQ.Text = "Diff Trans Sent/DR " & formatNumber(total7 - total8)
            lblDiffTransC.Text = "Diff Trans Sent/DR " & formatNumber(total11 - total12)
            lblChargedAmountA.Text = "Charged Amount (Bht) " & formatNumber(total5 * 3)
            lblChargedAmountQ.Text = "Charged Amount (Bht) " & formatNumber(total9 * 3)
            lblChargedAmountC.Text = "Charged Amount (Bht) " & formatNumber(total13 * 3)

            lblDiffTransA.Visible = True
            lblDiffTransQ.Visible = True
            lblDiffTransC.Visible = True
            lblChargedAmountA.Visible = True
            lblChargedAmountQ.Visible = True
            lblChargedAmountC.Visible = True
        End If
    End Sub

    Function formatNumber(number As Integer) As String
        Return Convert.ToDecimal(number).ToString("#,##0")
    End Function

    Private Sub GridView1_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GridView1.RowDataBound
        Dim gvr As GridViewRow = e.Row
        If gvr.RowType = DataControlRowType.DataRow Then
            total2 += Replace(e.Row.Cells(2).Text, ",", "")
            total3 += Replace(e.Row.Cells(3).Text, ",", "")
            total4 += Replace(e.Row.Cells(4).Text, ",", "")
        ElseIf gvr.RowType = DataControlRowType.Footer Then
            e.Row.Cells(1).Text = "Summary"
            e.Row.Cells(2).Text = formatNumber(total2)
            e.Row.Cells(3).Text = formatNumber(total3)
            'e.Row.Cells(4).Text = formatNumber(total4)
        End If
    End Sub

    Private Sub GridView2_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GridView2.RowDataBound
        Dim gvr As GridViewRow = e.Row
        If gvr.RowType = DataControlRowType.DataRow Then
            total1 += Replace(e.Row.Cells(1).Text, ",", "")
            total2 += Replace(e.Row.Cells(2).Text, ",", "")
            total3 += Replace(e.Row.Cells(3).Text, ",", "")
            total4 += Replace(e.Row.Cells(4).Text, ",", "")
        ElseIf gvr.RowType = DataControlRowType.Footer Then
            e.Row.Cells(0).Text = "Summary"
            e.Row.Cells(1).Text = formatNumber(total1)
            e.Row.Cells(2).Text = formatNumber(total2)
            e.Row.Cells(3).Text = formatNumber(total3)
            e.Row.Cells(4).Text = formatNumber(total4)
        End If
    End Sub

    Private Sub GridView3_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GridView3.RowDataBound
        Dim gvr As GridViewRow = e.Row
        If gvr.RowType = DataControlRowType.DataRow Then
            total3 += Replace(e.Row.Cells(3).Text, ",", "")
            total4 += Replace(e.Row.Cells(4).Text, ",", "")
            total5 += Replace(e.Row.Cells(5).Text, ",", "")
            total6 += Replace(e.Row.Cells(6).Text, ",", "")
        ElseIf gvr.RowType = DataControlRowType.Footer Then
            e.Row.Cells(0).Text = "Summary"
            e.Row.Cells(3).Text = formatNumber(total3)
            e.Row.Cells(4).Text = formatNumber(total4)
            e.Row.Cells(5).Text = formatNumber(total5)
            e.Row.Cells(6).Text = formatNumber(total6)
        End If
    End Sub
End Class