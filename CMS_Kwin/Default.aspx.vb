﻿Public Class _Default
    Inherits Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim Cookie As HttpCookie
        Dim intUsrId As Int16 = 0
        Try
            Cookie = Request.Cookies("USR")
            intUsrId = Cookie.Item("USR_ID")
            If (Request.Cookies("USR") IsNot Nothing) Then
                If (Request.Cookies("USR")("USR_ID") IsNot Nothing) Then
                    intUsrId = Request.Cookies("USR")("USR_ID")
                Else
                End If
            Else
            End If
        Catch ex As Exception
            'Response.Redirect("/Account/login.aspx")
        Finally

        End Try
        If intUsrId = 1 Or intUsrId = 3 Then
            Response.Redirect("/log_menu.aspx?mnu_id=1")
        ElseIf intUsrId = 2 Then
            Response.Redirect("/log_menu.aspx?mnu_id=2")
        ElseIf intUsrId = 4 Then
            Response.Redirect("/log_menu.aspx?mnu_id=5")
        Else
            Response.Redirect("/Account/login.aspx")
        End If
    End Sub
End Class