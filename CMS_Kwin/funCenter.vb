﻿Imports System.Web
Imports System.Data.SqlClient

Public Class funCenter
    Implements IHttpModule

    Private WithEvents _context As HttpApplication
    Private MyConn As SqlConnection
    Private objDs As DataSet
    Private Objrecordset As SqlDataAdapter
    Private MyCommand As SqlCommand
    Private strSql As String
    ''' <summary>
    '''  You will need to configure this module in the Web.config file of your
    '''  web and register it with IIS before being able to use it. For more information
    '''  see the following link: http://go.microsoft.com/?linkid=8101007
    ''' </summary>
#Region "IHttpModule Members"

    Public Sub Dispose() Implements IHttpModule.Dispose

        ' Clean-up code here

    End Sub

    Public Sub Init(ByVal context As HttpApplication) Implements IHttpModule.Init
        _context = context
    End Sub

#End Region

    Public Sub OnLogRequest(ByVal source As Object, ByVal e As EventArgs) Handles _context.LogRequest

        ' Handles the LogRequest event to provide a custom logging 
        ' implementation for it

    End Sub

    Public ReadOnly Property Connection() As SqlConnection
        Get
            MyConn = New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
            Return MyConn
        End Get
    End Property

    Public ReadOnly Property ReportConnection() As SqlConnection
        Get
            MyConn = New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("ReportConnection").ConnectionString)
            Return MyConn
        End Get
    End Property

    Public ReadOnly Property ReportPartnerConnection() As SqlConnection
        Get
            MyConn = New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("ReportPartnerConnection").ConnectionString)
            Return MyConn
        End Get
    End Property

    Public ReadOnly Property IsoftelConnection() As SqlConnection
        Get
            MyConn = New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("IsoftelConnection").ConnectionString)
            Return MyConn
        End Get
    End Property

    Public ReadOnly Property AzureConnection() As SqlConnection
        Get
            MyConn = New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("AzureConnection").ConnectionString)
            Return MyConn
        End Get
    End Property

    Function QueryData(ByVal strSql As String, ByVal vStrDs As String, _
                   Optional ByVal Database As String = "", _
                   Optional ByVal currentIndex As String = "", _
                   Optional ByVal PageSize As String = "", _
                   Optional ByVal DbConnection As String = "") As DataSet
        Try
            If DbConnection = "report" Then
                MyConn = ReportConnection()
            ElseIf DbConnection = "report_partner" Then
                MyConn = ReportPartnerConnection()
            ElseIf DbConnection = "azure" Then
                MyConn = AzureConnection()
            Else
                MyConn = Connection()
            End If
            If MyConn.State = ConnectionState.Closed Then
                MyConn.Open()
            End If

            Objrecordset = New SqlDataAdapter(strSql, MyConn)
            objDs = New DataSet
            If currentIndex <> "" And PageSize <> "" Then
                Objrecordset.Fill(objDs, currentIndex, PageSize, vStrDs)
            Else
                Objrecordset.Fill(objDs, vStrDs)
            End If
            MyConn.Close()

            Return objDs
        Catch
            MyConn.Close()
            Return Nothing
        End Try
    End Function

    Function executeData(ByVal strSql As String, Optional ByVal Database As String = "", Optional ByVal DbConnection As String = "")
        Try
            If DbConnection = "report" Then
                MyConn = ReportConnection()
            Else
                MyConn = Connection()
            End If

            If MyConn.State = ConnectionState.Closed Then
                MyConn.Open()
            End If

            MyCommand = New SqlCommand(strSql, MyConn)
            If MyConn.State = ConnectionState.Closed Then
                MyConn.Open()
            End If
            MyCommand.ExecuteNonQuery()
            MyConn.Close()
            Return "0"
        Catch
            MyConn.Close()
            Return Err.Description
        End Try
    End Function

    Public Function Data_dropdown(ByVal strSql As String, ByVal vStrKey As String, ByVal vStrValue As String, Optional ByVal vStrSearch As String = "N", _
Optional ByVal Database As String = "", Optional ByVal vStrNameSearch As String = "  Please selected  ")
        Dim Dr As DataRow
        Dim mySortedlist As SortedList
        Dim i As Integer = 1
        Try
            objDs = New DataSet
            objDs = QueryData(strSql, "Data", "", "", "", Database)
            mySortedlist = New SortedList

            If vStrSearch = "Y" Then
                mySortedlist.Add(vStrNameSearch, 0)
            End If
            For Each Dr In objDs.Tables("Data").Rows
                Try
                    mySortedlist.Add(Convert.ToString(Dr.Item(vStrKey)), Dr.Item(vStrValue))
                Catch
                    mySortedlist.Add(Convert.ToString(Dr.Item(vStrKey)) & "(" & i & ")", Dr.Item(vStrValue))
                    i += 1
                End Try
            Next
            Return mySortedlist
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Function GenDateTimeLog()
        Dim strDate As String
        Dim strMonth As String
        Dim strFileDate As String
        If (Len(DateTime.Now.Day.ToString) = 1) Then
            strDate = "0" & DateTime.Now.Day.ToString
        Else
            strDate = DateTime.Now.Day.ToString
        End If
        If (Len(DateTime.Now.Month.ToString) = 1) Then
            strMonth = "0" & DateTime.Now.Month.ToString
        Else
            strMonth = DateTime.Now.Month.ToString
        End If
        strFileDate = DateTime.Now.Year.ToString + strMonth + strDate + DateTime.Now.Hour.ToString + DateTime.Now.Minute.ToString + DateTime.Now.Second.ToString
        Return strFileDate
    End Function

    Function LogMenu(menu_id As Int16, usr_id As Int16) As String
        Try
            strSql = "INSERT INTO BACKOFF_MENU_STAT(MNU_ID,USR_ID) VALUES(" & menu_id & "," & usr_id & ")"
            Return executeData(strSql)
        Catch ex As Exception
            Return ex.Message
        Finally

        End Try
    End Function

    Function GetServiceName(intSvId As Int16) As String
        strSql = "SELECT TOP 1 SV_NAME" & _
                " FROM SERVICE" & _
                " WHERE SV_ID = " & intSvId
        objDs = QueryData(strSql, "DATA")
        If objDs.Tables("DATA").Rows.Count > 0 Then
            Return objDs.Tables("DATA").Rows(0).Item("SV_NAME")
        Else
            Return Nothing
        End If
    End Function

End Class
