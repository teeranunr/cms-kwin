﻿Imports System.Globalization
Public Class test3
    Inherits System.Web.UI.Page
    Private strSql As String
    Private funCenter As New funCenter
    Private objDs As DataSet
    Private strStartDate As String
    Private strEndDate As String
    Private intUserId As Integer
    Private strCultureInfo As CultureInfo = New CultureInfo("en-US")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Request.Cookies("USR") IsNot Nothing) Then
            If (Request.Cookies("USR")("PTN_ID") IsNot Nothing) Then
                intUserId = Request.Cookies("USR")("PTN_ID")
            End If
        End If

        If Not IsPostBack() Then
            'BindData()
        End If
    End Sub


    Sub BindData()
        Try
            strSql = "SELECT * FROM SEVEN_BILL"
            objDs = funCenter.QueryData(strSql, "DATA", "", "", "", "report_partner")
            If objDs.Tables("DATA").Rows.Count > 0 Then
                GridView1.DataSource = objDs.Tables("DATA")
                GridView1.DataBind()
                GridView1.Visible = True
                'lblError.Visible = False
                'btnExportExcel.Visible = True
            Else
                GridView1.DataSource = Nothing
                GridView1.Visible = False
                'lblError.Text = "No Data!"
                'lblError.Visible = True
                'btnExportExcel.Visible = False
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
End Class