﻿Imports System.IO
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports Excel

Public Class _case
    Inherits System.Web.UI.Page
    Private strSql As String
    Private funCenter As New funCenter
    Private MyConn As SqlConnection
    Private sqlTrans As SqlTransaction

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    

    Function GetFileDetail(ciqId As Integer)
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("IsoftelConnection").ConnectionString)
        conn.Open()
        strSql = "SELECT FILE_ORIGINAL_NAME,FILE_ACTUAL_NAME,FILE_CREATE_DATE,FILE_CREATE_DATE_TEXT" & _
                " FROM CASE_INQUIRY_FILE_UPLOAD WHERE CIQ_ID =" & ciqId

        Dim da As New SqlDataAdapter(strSql, conn)
        Dim ds As New DataSet()

        da.Fill(ds, "CONTENT")
        If ds.Tables("CONTENT").Rows.Count > 0 Then

        Else

        End If
        conn.Close()
    End Function

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Dim mobileNo As String = txbMobileNo.Text.Trim()
        Dim detail As String = txbDetail.Text.Trim()
        Dim status As Integer = 0
        Try
            MyConn = funCenter.IsoftelConnection()
            If MyConn.State = ConnectionState.Closed Then
                MyConn.Open()
            End If
            sqlTrans = MyConn.BeginTransaction
            strSql = "INSERT INTO CASE_INQUIRY(PHONE_NO,CIQ_DETAIL,CIQ_STATUS)" & _
                    " VALUES(@mobileNo,@detail,2)"
            Dim insertContent As New SqlCommand()
            insertContent.Connection = MyConn
            insertContent.Transaction = sqlTrans
            insertContent.CommandText = strSql
            insertContent.Parameters.Add(New SqlParameter("@mobileNo", mobileNo))
            insertContent.Parameters.Add(New SqlParameter("@detail", detail))
            insertContent.ExecuteNonQuery()
            sqlTrans.Commit()
            lblError.Text = "Add Data Success!"
            status = 1
        Catch ex As Exception
            lblError.Text = ex.Message
            'Response.Write(ex.Message)
        Finally

        End Try
        If MyConn.State = ConnectionState.Open Then
            MyConn.Close()
        End If
        txbMobileNo.Text = ""
        txbDetail.Text = ""
        'If status = 1 Then
        '    Response.Redirect("case_detail.aspx")
        'End If
    End Sub

    Protected Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click
        txbMobileNo.Text = ""
        txbDetail.Text = ""
    End Sub
End Class