﻿Imports System.Data.SqlClient

Public Class info
    Inherits System.Web.UI.Page
    Private funCenter As New funCenter
    Private strSql As String
    Private objDs As DataSet
    Private strMobileNo As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack() Then
            'BindData()
            btnSubmit.Visible = False
        End If
    End Sub

    Public Sub BindData()
        strMobileNo = "66" & Right(txbMobileNo.Text.Trim(), 9)
        'If strMobileNo <> "66" Then
        'Try
        strSql = "SELECT TOP 10 B.OPT_CODE,B.SUB_ID,B.SUBL_ID,B.SV_ID,S.SV_NAME," & _
                 " B.STATUS,ISNULL(CONVERT(varchar, B.CREATE_DATE, 120), '') AS CREATE_DATE," & _
                 " ISNULL(CONVERT(varchar, B.END_DATE, 120), '') AS END_DATE," & _
                 " ISNULL(CONVERT(varchar, B.CANCEL_DATE, 120), '') AS CANCEL_DATE," & _
                 " ISNULL(B.REMARK,'') AS REMARK,ISNULL(B.SUB_CHANNEL,'') AS SUB_CHANNEL," & _
                 " ISNULL(B.CANCEL_CHANNEL,'') AS CANCEL_CHANNEL,ISNULL(B.OPT_LINKED_ID,'') AS OPT_LINKED_ID" & _
                 " FROM SUBSCRIPTION B,SERVICE S" & _
                 " WHERE S.SV_ID = B.SV_ID" & _
                 " AND B.PHONE_NO = '" & strMobileNo & "'" & _
                 " ORDER BY B.STATUS ASC,B.CREATE_DATE DESC"
        objDs = funCenter.QueryData(strSql, "DATA", "GAME")
        If objDs.Tables("DATA").Rows.Count > 0 Then
            GridView1.DataSource = objDs.Tables("DATA")
            GridView1.DataBind()
            'For Each rowItem As GridViewRow In GridView1.Rows
            '    rowItem.Attributes.Add("onmouseover", "javascript:this.className = 'rowStyle'")
            '    rowItem.Attributes.Add("onmouseout", "javascript:this.className = ''")
            'Next
            btnSubmit.Visible = True
            lblError.Visible = False
            GridView1.Visible = True
        Else
            GridView1.DataSource = Nothing
            GridView1.Visible = False
            btnSubmit.Visible = False
            lblError.Text = "No Data!"
            lblError.Visible = True
        End If
        'Catch ex As Exception
        '    Response.Write(ex.Message & strSql)
        'End Try

        'Else
        'btnSubmit.Visible = False
        'lblError.Visible = False
        'End If
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        BindData()
    End Sub

    Protected Sub GridView1_PageIndexChanging(sender As Object, e As GridViewPageEventArgs)

    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Dim str As New StringBuilder()
        Dim strSvId As String
        Dim strOptCode As String
        Dim MyConn As SqlConnection
        Dim sqlTrans As SqlTransaction
        For i As Integer = 0 To GridView1.Rows.Count - 1
            Dim row As GridViewRow = GridView1.Rows(i)
            Dim isChecked As Boolean = DirectCast(row.FindControl("ChkCancel"), CheckBox).Checked
            If isChecked Then
                Try
                    Dim strServiceCode As String
                    strSvId = GridView1.DataKeys(i).Value.ToString
                    strServiceCode = GetServiceCode(strSvId)
                    strMobileNo = "66" & Right(txbMobileNo.Text.Trim(), 9)
                    strSql = "SELECT OPT_CODE FROM SUBSCRIPTION WHERE PHONE_NO = '" & strMobileNo & "' AND SV_ID =" & strSvId
                    objDs = funCenter.QueryData(strSql, "DATA", "GAME")
                    If objDs.Tables("DATA").Rows.Count > 0 Then
                        strOptCode = objDs.Tables("DATA").Rows(0).Item("OPT_CODE")
                    Else
                        strOptCode = "01"
                    End If

                    MyConn = funCenter.Connection()
                    MyConn.Open()
                    sqlTrans = MyConn.BeginTransaction
                    Dim usp_ReqService As New SqlCommand()
                    usp_ReqService.Connection = MyConn
                    usp_ReqService.Transaction = sqlTrans

                    strSql = "EXEC [VTRAN_3].[dbo].[usp_Backoffice_ReqTrans] 'unsub',@mobileNo, @opt_code,@sv_id"

                    usp_ReqService.CommandText = strSql
                    usp_ReqService.Parameters.Add(New SqlParameter("@mobileNo", strMobileNo))
                    usp_ReqService.Parameters.Add(New SqlParameter("@opt_code", strOptCode))
                    usp_ReqService.Parameters.Add(New SqlParameter("@sv_id", strSvId))
                    usp_ReqService.ExecuteNonQuery()
                    sqlTrans.Commit()
                    MyConn.Close()
                Catch ex As Exception
                    Response.Write(ex.Message)
                End Try
                'Dim webClient As New System.Net.WebClient
                'Dim result As String = webClient.DownloadString("http://202.43.45.148/sms/callcenter_sub.ashx?msisdn=" & strMobileNo & "&action=unsub&sv=" & strServiceCode)
            End If
        Next
        BindData()
        lblError.Visible = True
        lblError.Text = "Unsub Success."
    End Sub

    Function CheckStatus(status As String) As String
        If status = "0" Then
            Return "Active"
        Else
            Return "inactive"
        End If
    End Function

    Function GetServiceCode(svId As String)
        strSql = "SELECT TOP 1 SV_ID,(APP_ID + REG_ID) AS SERVICE_CODE" & _
                " FROM LPLUS_SERVICE_NUMBER" & _
                " WHERE SV_ID = " & svId
        objDs = funCenter.QueryData(strSql, "DATA")
        If objDs.Tables("DATA").Rows.Count > 0 Then
            Return objDs.Tables("DATA").Rows(0).Item("SERVICE_CODE")
        Else
            Return Nothing
        End If
    End Function

    Function GetVisible(status As Int16) As Boolean
        If status = 0 Then
            Return True
        Else
            Return False
        End If
    End Function
End Class