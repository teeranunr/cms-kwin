﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="info.aspx.vb" Inherits="CMS_Kwin.info" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
      <div class="col-md-12" style="height:10px;" >

      </div>  
    </div> 
    <div class="row">
        <div class="col-md-6" style="text-align:right">
            <asp:Label ID="Label1" runat="server" Text="Mobile No."></asp:Label>
        </div>
        <div class="col-md-6" style="text-align:left">
            <asp:TextBox ID="txbMobileNo" runat="server" MaxLength="10" Text="0"></asp:TextBox>
        </div>  
    </div>
    <div class="row">
        <div class="col-md-12" style="height:10px;" >

      </div>
    </div> 
    <div class="row">
        <div class="col-md-6" style="text-align:right">
            
        </div>
        <div class="col-md-6" style="text-align:left">
            <asp:Button ID="btnSearch" runat="server" Text="SEARCH" CssClass="btn btn-default"/>
        </div>  
    </div>
    <div class="row">
        <div class="col-md-12" style="height:10px;" >

        </div>
    </div> 
    <div class="row">
        <div class="col-md-6" style="text-align:right">
            
        </div>
        <div class="col-md-6" style="text-align:left">
            <asp:Label ID="lblError" runat="server" Text="" Font-Bold="true" ForeColor="Red"></asp:Label>
        </div>  
    </div>
    <hr />
    <div>
        
        <asp:GridView ID="GridView1" runat="server" BackColor="White" BorderColor="#999999" 
            BorderStyle="Solid" BorderWidth="1px" CellPadding="3" ForeColor="Black" 
            GridLines="Vertical" AutoGenerateColumns="False" PageSize="20" AllowPaging="True" 
            HeaderStyle-Font-Size="10px"
                OnPageIndexChanging="GridView1_PageIndexChanging" Width="100%" DataKeyNames="SV_ID">
            <AlternatingRowStyle BackColor="#CCCCCC" />
            <Columns>                
                <asp:TemplateField HeaderText="SERVICE" ItemStyle-Width="20%">
                    <itemtemplate>
					    <%# Container.DataItem("SV_NAME")%>
				    </itemtemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="CREATE DATE" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Center">
                    <itemtemplate>
					    <%# Container.DataItem("CREATE_DATE")%>
				    </itemtemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="CANCEL DATE" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Center">
                    <itemtemplate>
					    <%# Container.DataItem("CANCEL_DATE")%>
				    </itemtemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="STATUS" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
                    <itemtemplate>
					    <%# CheckStatus(Container.DataItem("STATUS"))%>
				    </itemtemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="CANCEL" ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Center">
                    <itemtemplate>
					    <asp:CheckBox ID ="ChkCancel" runat="server" Visible='<%#GetVisible(Container.DataItem("STATUS"))%>' />
				    </itemtemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="REMARK" ItemStyle-Width="10%">
                    <itemtemplate>
					    <%# Container.DataItem("REMARK")%>
				    </itemtemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="SUB CHANNEL" ItemStyle-Width="10%">
                    <itemtemplate>
					    <%# Container.DataItem("SUB_CHANNEL")%>
				    </itemtemplate>
                </asp:TemplateField>
                                <asp:TemplateField HeaderText=" CANCEL CHANNEL" ItemStyle-Width="10%">
                    <itemtemplate>
					    <%# Container.DataItem("CANCEL_CHANNEL")%>
				    </itemtemplate>
                </asp:TemplateField>
            </Columns>
            <FooterStyle BackColor="#CCCCCC" />
            <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#808080" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#383838" />
        </asp:GridView>
    </div>
    <div class="row">
        <div class="col-md-12" style="height:10px;">

        </div>
    </div>
    <div class="row">
        <div class="col-md-6" style="text-align:right">
            
        </div>
        <div class="col-md-6" style="text-align:left">
            <asp:Button ID="btnSubmit" runat="server" Text="SUBMIT" CssClass="btn btn-default"/> 
        </div>  
    </div>    
</asp:Content>
