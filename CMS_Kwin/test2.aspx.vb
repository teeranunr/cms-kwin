﻿Public Class test2
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If NewRandom.PercentChance(10) Then
            Response.Write("10")
        ElseIf NewRandom.PercentChance(50) Then
            Response.Write("50")
        Else
            Response.Write("40")
        End If
    End Sub

    Public Class NewRandom
        Private Shared _rnd As New Random()
        Public Shared Function PercentChance(percent As Integer) As Boolean
            Dim d As Double = CDbl(percent) / 100.0
            Return (_rnd.NextDouble() <= d)
        End Function
    End Class
End Class