﻿Imports System.IO
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports Excel

Public Class import_game
    Inherits System.Web.UI.Page
    Private strSql As String
    Private funCenter As New funCenter
    Private intSvId As Int32
    Private strResponse As String
    Private MyConn As SqlConnection
    Private sqlTrans As SqlTransaction

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack() Then
            strSql = "SELECT SV_ID,SV_NAME FROM SERVICE"
            DropDownList1.DataSource = funCenter.Data_dropdown(strSql, "SV_NAME", "SV_ID", "Y")
            DropDownList1.DataTextField = "key"
            DropDownList1.DataValueField = "value"
            DropDownList1.DataBind()
            BindData()
        End If
    End Sub

    Public Sub BindData()
        intSvId = DropDownList1.SelectedValue()
        If intSvId > 0 Then
            Try
                Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
                conn.Open()
                strSql = "SELECT TOP 1000 C.CNT_ID,C.SV_ID,S.SV_NAME,C.CNT_DESC,C.CNT_CHOICE," & _
                        "C.CNT_RESULT,C.CNT_ANSWER," & _
                        " RIGHT('00'+CAST(datepart(dd,C.SCHEDULE_DATE) AS varchar(20)),2) + '-'+ " & _
                        " RIGHT('00'+CAST(datepart(mm,C.SCHEDULE_DATE) AS varchar(20)),2) + '-' + " & _
                        " CAST(datepart(yyyy,C.SCHEDULE_DATE) AS varchar(20))  + ' ' + " & _
                        " RIGHT('00'+CAST(datepart(hh,C.SCHEDULE_DATE) AS varchar(20)),2) + ':'+ " & _
                        " RIGHT('00'+CAST(datepart(mi,C.SCHEDULE_DATE) AS varchar(20)),2) + ':' + " & _
                        " RIGHT('00'+CAST(datepart(ss,C.SCHEDULE_DATE) AS varchar(20)),2) AS SCHEDULE_DATE," & _
                        " RIGHT('00'+CAST(datepart(dd,C.CREATE_DATE) AS varchar(20)),2) + '-'+" & _
                        " RIGHT('00'+CAST(datepart(mm,C.CREATE_DATE) AS varchar(20)),2) + '-' +" & _
                        " CAST(datepart(yyyy,C.CREATE_DATE) AS varchar(20))  + ' ' + " & _
                        " RIGHT('00'+CAST(datepart(hh,C.CREATE_DATE) AS varchar(20)),2) + ':'+ " & _
                        " RIGHT('00'+CAST(datepart(mi,C.CREATE_DATE) AS varchar(20)),2) + ':' + " & _
                        " RIGHT('00'+CAST(datepart(ss,C.CREATE_DATE) AS varchar(20)),2) AS CREATE_DATE,C.STATUS" & _
                        " FROM CONTENT C,SERVICE S" & _
                        " WHERE S.SV_ID = C.SV_ID AND C.SV_ID = " & intSvId & _
                        " ORDER BY C.CNT_ID DESC"
                Dim da As New SqlDataAdapter(strSql, conn)
                Dim ds As New DataSet()
                da.Fill(ds, "CONTENT")
                If ds.Tables("CONTENT").Rows.Count > 0 Then
                    GridView1.DataSource = ds.Tables("CONTENT")
                    GridView1.DataBind()
                    GridView1.Visible = True
                    lblError.Visible = False
                Else
                    lblError.Visible = True
                    lblError.Text = "No Data!"
                    GridView1.DataSource = Nothing
                    GridView1.Visible = False
                End If
                conn.Close()
            Catch ex As Exception
                Response.Write(ex.Message)
            Finally

            End Try
            lblSelectFile.Visible = True
            FileUpload1.Visible = True
            Button1.Visible = True
            lblRemark.Visible = True
        Else
            lblSelectFile.Visible = False
            FileUpload1.Visible = False
            Button1.Visible = False
            lblRemark.Visible = False
        End If
    End Sub

    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        intSvId = DropDownList1.SelectedValue()
        If intSvId > 0 Then
            If FileUpload1.HasFile Then
                Dim FileName As String = Path.GetFileName(FileUpload1.PostedFile.FileName)
                Dim Extension As String = Path.GetExtension(FileUpload1.PostedFile.FileName)
                Dim FolderPath As String = ConfigurationManager.AppSettings("FolderPath")
                Dim FolderLogPath As String = ConfigurationManager.AppSettings("FolderLogPath")
                If Extension = ".xls" Or Extension = ".xlsx" Then
                    Dim FilePath As String = Server.MapPath(FolderPath + FileName)
                    FileUpload1.SaveAs(FilePath)
                    FileUpload1.SaveAs(Server.MapPath(FolderLogPath + funCenter.GenDateTimeLog() & "_" & FileName))
                    Dim stream As FileStream = File.Open(FilePath, FileMode.Open, FileAccess.Read)
                    Dim excelReader As IExcelDataReader
                    If Extension = ".xls" Then
                        excelReader = ExcelReaderFactory.CreateBinaryReader(stream)
                    Else
                        excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream)
                    End If
                    Dim result As DataSet = excelReader.AsDataSet()
                    Dim strDate As String = ""
                    Dim question As String = ""
                    Dim answer As String = ""
                    Dim choice As String = ""
                    Dim choice_answer As String = ""
                    Dim i As Int16 = 0
                    Dim strMonth As String = Left(FileName, 2)
                    Dim strYear As String = Mid(FileName, 3, 4)
                    While excelReader.Read()
                        If i = 0 Then
                        Else
                            strDate = excelReader(0)
                            question = excelReader(1)
                            choice = excelReader(2)
                            answer = excelReader(3)
                            choice_answer = excelReader(4)
                            If intSvId > 0 And question <> "" And choice <> "" And choice_answer <> "" And strYear <> "" And strMonth <> "" And strDate <> "" Then
                                Insert_content(intSvId, question, choice, answer, choice_answer, strYear, strMonth, strDate)
                            Else

                            End If
                        End If
                        i = i + 1
                    End While
                    File.Delete(FilePath)
                    excelReader.Close()
                    lblError.Text = "Import complete."
                    lblError.Visible = True
                Else
                    lblError.Text = "File format not support."
                    lblError.Visible = True
                End If
            Else
                lblError.Text = "Please select a file to import."
                lblError.Visible = True
            End If
        Else

        End If
        BindData()
    End Sub

    Public Sub Insert_content(sv_id As Int32, question As String, choice As String, answer As String, _
                              choice_answer As String, strYear As String, strMonth As String, strDay As String)
        Dim schedule_date As String
        If strDay.Length = 1 Then
            strDay = Left("0" & strDay, 2)
        End If
        schedule_date = " convert(datetime ,'" & strYear & "-" & strMonth & "-" & strDay & " 08:00')"
        'Response.Write(schedule_date)
        Try
            MyConn = funCenter.Connection()
            If MyConn.State = ConnectionState.Closed Then
                MyConn.Open()
            End If
            sqlTrans = MyConn.BeginTransaction
            strSql = "INSERT INTO CONTENT(SV_ID,CNT_DESC,CNT_CHOICE,CNT_RESULT,CNT_ANSWER,SCHEDULE_DATE)" & _
                    " VALUES(@svId,@question,@choice,@answer,@choice_answer," & schedule_date & ")"
            Dim insertContent As New SqlCommand()
            insertContent.Connection = MyConn
            insertContent.Transaction = sqlTrans
            insertContent.CommandText = strSql
            insertContent.Parameters.Add(New SqlParameter("@svId", sv_id))
            insertContent.Parameters.Add(New SqlParameter("@question", question))
            insertContent.Parameters.Add(New SqlParameter("@choice", choice))
            insertContent.Parameters.Add(New SqlParameter("@answer", answer))
            insertContent.Parameters.Add(New SqlParameter("@choice_answer", choice_answer))
            'insertContent.Parameters.Add(New SqlParameter("@schedule_date", schedule_date))
            insertContent.ExecuteNonQuery()
            sqlTrans.Commit()
        Catch ex As Exception
            Response.Write(ex.Message & schedule_date)
        Finally

        End Try
        If MyConn.State = ConnectionState.Open Then
            MyConn.Close()
        End If
    End Sub

    Protected Sub DropDownList1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DropDownList1.SelectedIndexChanged
        BindData()
    End Sub

    Protected Sub GridView1_RowDeleting(sender As Object, e As GridViewDeleteEventArgs)
        Try
            Dim intCntId As Integer
            intCntId = e.Keys("CNT_ID")
            If intCntId > 0 Then
                MyConn = funCenter.Connection()
                If MyConn.State = ConnectionState.Closed Then
                    MyConn.Open()
                End If
                sqlTrans = MyConn.BeginTransaction
                strSql = "DELETE CONTENT WHERE CNT_ID = @cntId"
                Dim deleteContent As New SqlCommand()
                deleteContent.Connection = MyConn
                deleteContent.Transaction = sqlTrans
                deleteContent.CommandText = strSql
                deleteContent.Parameters.Add(New SqlParameter("@cntId", intCntId))
                deleteContent.ExecuteNonQuery()
                sqlTrans.Commit()
            Else

            End If
            'Response.Write(intCntId)
        Catch ex As Exception
            Response.Write(ex.Message)
        Finally

        End Try

        If MyConn.State = ConnectionState.Open Then
            MyConn.Close()
        End If
        BindData()
    End Sub

    Protected Sub GridView1_RowDeleted(sender As Object, e As GridViewDeletedEventArgs)
        Response.Write("GridView1_RowDeleted")
    End Sub

    Protected Sub GridView1_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            ' reference the Delete LinkButton
            Dim db As LinkButton = DirectCast(e.Row.Cells(7).Controls(0), LinkButton)
            db.OnClientClick = "return confirm('Delete this data confirm!');"
        End If
    End Sub

    Protected Sub GridView1_PageIndexChanging(sender As Object, e As GridViewPageEventArgs)
        GridView1.PageIndex = e.NewPageIndex
        intSvId = DropDownList1.SelectedValue()
        BindData()
    End Sub

    Function GetStatus(status As String) As String
        If status = "2" Or status = "3" Then
            Return "Send Success"
        Else
            Return "Waiting"
        End If
    End Function
End Class