﻿Imports System.Globalization
Imports System.Drawing
Imports System.Data.SqlClient
Public Class truemove_dr
    Inherits System.Web.UI.Page
    Private objDs As DataSet
    Private strSvId As String
    Private strSql As String
    Private strAppId As String
    Private strWhere As String
    Private funCenter As New funCenter
    Dim total1 As Int32 = 0
    Dim total2 As Int32 = 0
    Dim total3 As Int32 = 0
    Dim total4 As Int32 = 0
    Dim total5 As Int32 = 0
    Dim total6 As Int32 = 0
    Dim total7 As Int32 = 0
    Public totalDr000 As Int32
    Public totalDlvr000 As Int32
    Private strCultureInfo As CultureInfo = New CultureInfo("en-US")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack() Then
            cldStart.Visible = False
            cldEnd.Visible = False
            Dim dateNow As DateTime = DateTime.Now.AddDays(-1)
            txbStart.Text = dateNow.Year & Right("0" & dateNow.Month, 2) & Right("0" & dateNow.Day, 2)
            txbEnd.Text = dateNow.Year & Right("0" & dateNow.Month, 2) & Right("0" & dateNow.Day, 2)

            strSql = "SELECT DISTINCT APP_ID FROM PARTNER_SERVICE"
            ddlAppId.DataSource = funCenter.Data_dropdown(strSql, "APP_ID", "APP_ID", "Y", "report_partner", " ALL ")
            ddlAppId.DataTextField = "key"
            ddlAppId.DataValueField = "value"
            ddlAppId.DataBind()

            strSql = "SELECT p.SV_ID,s.SV_NAME FROM PARTNER_SERVICE p LEFT OUTER JOIN VCONT.dbo.SERVICE s ON p.SV_ID = s.SV_ID WHERE P.SV_ID IN(41,20)"
            ddlServicePartner.DataSource = funCenter.Data_dropdown(strSql, "SV_NAME", "SV_ID", "Y", "report_partner", " ALL ")
            ddlServicePartner.DataTextField = "key"
            ddlServicePartner.DataValueField = "value"
            ddlServicePartner.DataBind()
            BindData()
        End If
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        BindData()
    End Sub

    Sub BindData()
        strAppId = ddlAppId.SelectedValue()
        strSvId = ddlServicePartner.SelectedValue()

        ddlAppId.SelectedValue = strAppId
        ddlServicePartner.SelectedValue = strSvId

        If strAppId <> "0" And strAppId <> "" Then
            strWhere = strWhere & " AND SV_ID IN(SELECT DISTINCT SV_ID FROM PARTNER_SERVICE WHERE APP_ID = " & strAppId & ")"
        End If

        If strSvId <> "0" And strSvId <> "" Then
            strWhere = strWhere & " AND SV_ID = '" & strSvId & "' "
        End If
        Try
            If txbStart.Text.Trim() <> "" And txbEnd.Text.Trim() <> "" Then
                strSql = "SELECT YYYYMMDD,SV_ID,SUBS_NUM,UNSUBS_NUM,ISNULL(TOTAL_TRANS,0) AS TOTAL_TRANS," &
                        "ISNULL(TOTAL_SENT_TRANS,0) AS TOTAL_SENT_TRANS,ISNULL(TOTAL_AMOUNT,0) AS TOTAL_AMOUNT," &
                        "TOTAL_AMOUNT_SENT_DLVR_000," &
                        "TOTAL_AMOUNT_SENT_000,DR_SENT_000,DR_SENT_003,DR_SENT_004,DR_SENT_102,DR_SENT_410,DR_SENT_412," &
                        "DR_SENT_413,DR_SENT_OTHER,DR_SENT_OTHER_REMARK,DR_SENT_DLVR_000,DR_SENT_DLVR_001,DR_SENT_DLVR_200," &
                        "DR_SENT_DLVR_201,DR_SENT_DLVR_300,DR_SENT_DLVR_301,DR_SENT_DLVR_302,DR_SENT_DLVR_303,DR_SENT_DLVR_304," &
                        "DR_SENT_DLVR_OTHER,DIFF_DR_SENT_000_WITH_ALL_DLVR" &
                        " FROM VREPORT_2.dbo.REP_SUBS_TRANS_DR_PARTNER" &
                        " WHERE OPT_CODE = '04'" &
                        " AND YYYYMMDD BETWEEN '" & txbStart.Text.Trim() & "' AND '" & txbEnd.Text.Trim() & "' " & strWhere
                objDs = funCenter.QueryData(strSql, "DATA", "", "", "", "report_partner")
                If objDs.Tables("DATA").Rows.Count > 0 Then
                    GridView1.DataSource = objDs.Tables("DATA")
                    GridView1.DataBind()
                    GridView1.Visible = True
                    lblError.Visible = False
                Else
                    GridView1.DataSource = Nothing
                    GridView1.Visible = False
                    lblError.Text = "No Data!"
                    lblError.Visible = True
                    lblDiffRev.Visible = False
                    lbDiffRev2.Visible = False
                End If

                'Dim MyConn As SqlConnection
                'MyConn = funCenter.ReportPartnerConnection()
                'If MyConn.State = ConnectionState.Closed Then
                '    MyConn.Open()
                'End If
                'strSql = "SELECT SUM(DR_SENT_000) AS SUM_DR_000,SUM(DR_SENT_DLVR_000) AS SUM_DLVR_000" & _
                '        " FROM VREPORT_2.dbo.REP_SUBS_TRANS_DR_PARTNER" & _
                '        " WHERE OPT_CODE = '04'" & _
                '        " AND YYYYMMDD BETWEEN '" & txbStart.Text.Trim() & "' AND '" & txbEnd.Text.Trim() & "' " & strWhere
                'Dim command As New SqlCommand()
                'command.Connection = MyConn
                'command.CommandText = strSql
                'Dim reader As SqlDataReader = command.ExecuteReader()
                'If reader.Read() Then
                '    totalDr000 = reader.GetInt32(0)
                '    totalDlvr000 = reader.GetInt32(1)
                'End If
            End If
        Catch ex As Exception
            Response.Write(strSql & ":" & ex.Message)
        End Try
        total5 = 1000
    End Sub

    Protected Sub btnStart_Click(sender As Object, e As EventArgs) Handles btnStart.Click
        cldStart.Visible = True
        'BindData()
    End Sub

    Protected Sub btnEnd_Click(sender As Object, e As EventArgs) Handles btnEnd.Click
        cldEnd.Visible = True
        'BindData()
    End Sub

    Protected Sub cldStart_SelectionChanged(sender As Object, e As EventArgs) Handles cldStart.SelectionChanged
        cldStart.Visible = False
        cldEnd.Visible = False
        txbStart.Text = cldStart.SelectedDate.ToString("yyyyMMdd", strCultureInfo)
        BindData()
    End Sub

    Protected Sub cldEnd_SelectionChanged(sender As Object, e As EventArgs) Handles cldEnd.SelectionChanged
        cldStart.Visible = False
        cldEnd.Visible = False
        txbEnd.Text = cldEnd.SelectedDate.ToString("yyyyMMdd", strCultureInfo)
        BindData()
    End Sub

    'Private Sub txbStart_TextChanged(sender As Object, e As EventArgs) Handles txbStart.TextChanged
    '    cldStart.Visible = True
    'End Sub

    Protected Sub GridView1_DataBound(sender As Object, e As EventArgs)
        Try
            Dim row As New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal)
            Dim cell As New TableHeaderCell()

            cell = New TableHeaderCell()
            cell.Text = "Sent"
            cell.ColumnSpan = 8
            row.Controls.Add(cell)

            cell = New TableHeaderCell()
            cell.ColumnSpan = 10
            cell.Text = "Sent Delivered"
            row.Controls.Add(cell)

            row.BackColor = ColorTranslator.FromHtml("#ffffff")
            row.ForeColor = ColorTranslator.FromHtml("#000000")
            GridView1.HeaderRow.Parent.Controls.AddAt(0, row)

            Dim row2 As New GridViewRow(1, 0, DataControlRowType.Header, DataControlRowState.Normal)
            Dim cell2 As New TableHeaderCell()
            cell2.Text = ""
            cell2.ColumnSpan = 9
            cell2.RowSpan = 2
            row2.Controls.Add(cell2)

            cell2 = New TableHeaderCell()
            cell2.Text = "DR Type"
            cell2.ColumnSpan = 18
            row2.Controls.Add(cell2)

            cell2 = New TableHeaderCell()
            cell2.Text = ""
            cell2.RowSpan = 2
            row2.Controls.Add(cell2)
            row2.BackColor = ColorTranslator.FromHtml("#ffffff")
            row2.ForeColor = ColorTranslator.FromHtml("#000000")
            GridView1.HeaderRow.Parent.Controls.AddAt(0, row2)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Function GetServiceName(intSvId As Int16) As String
        Return funCenter.GetServiceName(intSvId)
    End Function

    Private Sub GridView1_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GridView1.RowDataBound
        Dim gvr As GridViewRow = e.Row
        If gvr.RowType = DataControlRowType.DataRow Then
            total1 += Replace(e.Row.Cells(2).Text, ",", "")
            total2 += Replace(e.Row.Cells(6).Text, ",", "")
            total3 += Replace(e.Row.Cells(7).Text, ",", "")
            total4 += Replace(e.Row.Cells(8).Text, ",", "")
            'total5 += Replace(e.Row.Cells(9).Text, ",", "")
            'total6 += Replace(e.Row.Cells(17).Text, ",", "")
        ElseIf gvr.RowType = DataControlRowType.Footer Then
            e.Row.Cells(0).Text = "Summary"
            e.Row.Cells(2).Text = String.Format("{0:#,###0}", Convert.ToDecimal(total1))
            e.Row.Cells(6).Text = String.Format("{0:#,###0}", Convert.ToDecimal(total2))
            e.Row.Cells(6).ForeColor = ColorTranslator.FromHtml("#FEDA9F")
            e.Row.Cells(7).Text = String.Format("{0:#,###0}", Convert.ToDecimal(total3))
            e.Row.Cells(8).Text = String.Format("{0:#,###0}", Convert.ToDecimal(total4))
            e.Row.Cells(8).BackColor = ColorTranslator.FromHtml("#FFCCEC")
            e.Row.Cells(8).ForeColor = ColorTranslator.FromHtml("#000000")
            'e.Row.Cells(9).Text = total5
            'e.Row.Cells(9).Text = String.Format("{0:#,###0}", Convert.ToDecimal(total5))
            e.Row.Cells(9).ForeColor = ColorTranslator.FromHtml("#FFCCEC")
            'e.Row.Cells(17).Text = String.Format("{0:#,###0}", Convert.ToDecimal(total6))
            e.Row.Cells(17).ForeColor = ColorTranslator.FromHtml("#4193FC")
            e.Row.Cells(0).Text = "Summary"
            lblDiffRev.Text = "Rev Log - Rev Backoffice = " & formatNumber(total3 - total2)
            lblDiffRev.Visible = True
            lbDiffRev2.Text = "Rev *** - Rev Log = " & formatNumber(total4 - total3)
            lbDiffRev2.Visible = True
        End If
    End Sub

    Function formatNumber(number As Integer) As String
        Return Convert.ToDecimal(number).ToString("#,##0")
    End Function

    Function GetTotalDR(strType As String) As String
        Try
            Dim MyConn As SqlConnection
            MyConn = funCenter.ReportPartnerConnection()
            If MyConn.State = ConnectionState.Closed Then
                MyConn.Open()
            End If
            strAppId = ddlAppId.SelectedValue()
            strSvId = ddlServicePartner.SelectedValue()

            If strAppId <> "0" And strAppId <> "" Then
                strWhere = strWhere & " AND SV_ID IN(SELECT DISTINCT SV_ID FROM PARTNER_SERVICE WHERE APP_ID = " & strAppId & ")"
            End If

            If strSvId <> "0" And strSvId <> "" Then
                strWhere = strWhere & " AND SV_ID = '" & strSvId & "' "
            End If
            strSql = "SELECT SUM(DR_SENT_000) AS SUM_DR_000,SUM(DR_SENT_DLVR_000) AS SUM_DLVR_000" & _
                            " FROM VREPORT_2.dbo.REP_SUBS_TRANS_DR_PARTNER" & _
                            " WHERE OPT_CODE = '04'" & _
                            " AND YYYYMMDD BETWEEN '" & txbStart.Text.Trim() & "' AND '" & txbEnd.Text.Trim() & "' " & strWhere
            'Response.Write(strSql)
            Dim command As New SqlCommand()
            command.Connection = MyConn
            command.CommandText = strSql
            Dim reader As SqlDataReader = command.ExecuteReader()
            If reader.Read() Then
                totalDr000 = reader.GetInt32(0)
                totalDlvr000 = reader.GetInt32(1)
            Else
                totalDr000 = 0
                totalDlvr000 = 0
            End If
            If strType = "DR" Then
                Return formatNumber(totalDr000)
            Else
                Return formatNumber(totalDlvr000)
            End If
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function
End Class