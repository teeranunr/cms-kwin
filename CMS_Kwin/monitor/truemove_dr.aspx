﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="truemove_dr.aspx.vb" Inherits="CMS_Kwin.truemove_dr" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
         <div class="row">
      <div class="col-md-12" style="height:10px;" >

      </div>  
    </div> 
    <div class="row">
        <div class="col-xs-12 col-md-12 col-sm-12 text-center" style="margin-top:10px;">
            <asp:Label ID="Label2" runat="server" Text="START DATE: "></asp:Label>
            <asp:TextBox ID="txbStart" runat="server"></asp:TextBox>
            <asp:Button ID="btnStart" runat="server" Text="..." />
            <div id="divCalendar" style="POSITION: absolute; z-index:10; left: 30%; top: 50%; visibility: visible;">
                <asp:Calendar ID="cldStart" runat="server" BackColor="White"></asp:Calendar>                 
            </div> &nbsp;&nbsp; 
                        <asp:Label ID="Label1" runat="server" Text="END DATE: "></asp:Label>
            <asp:TextBox ID="txbEnd" runat="server" ></asp:TextBox>
            <asp:Button ID="btnEnd" runat="server" Text="..." />
            <div id="div1" style="POSITION:absolute; z-index:10; left:50%; top:50%; visibility:visible;">
                <asp:Calendar ID="cldEnd" runat="server" BackColor="White"></asp:Calendar>  
            </div> 
        </div>
<%--        <div class="col-md-6" style="text-align:left">
          
        </div>--%>
    </div>
    <br />
        <div class="row">
         <div class="col-xs-12 col-md-12 col-sm-12 text-center" style="text-align:center">
            <asp:Label ID="lblAppId" runat="server" Text="  APP ID: "></asp:Label>
            <asp:DropDownList ID="ddlAppId" runat="server"></asp:DropDownList> &nbsp;&nbsp; 
            <asp:Label ID="lblServicePartner" runat="server" Text="  SERVICE: "></asp:Label>
            <asp:DropDownList ID="ddlServicePartner" runat="server"></asp:DropDownList>
        </div>
<%--            <div class="col-md-6" style="text-align:left">

            </div>--%>
    </div>
    <br />
    <div class="row">
        <div class="col-md-12" style="text-align:center">
            <asp:Button ID="btnSearch" runat="server" Text="SEARCH" CssClass="btn btn-default"/>
        </div> 
    </div>
    <br />
     <div class="row">
        <div class="col-md-12" style="text-align:center">
            <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
        </div> 
    </div>
    <br />
                <asp:GridView ID="GridView1" Width="100%" runat="server" AutoGenerateColumns="False" 
                HeaderStyle-ForeColor="Black" Visible="false" 
                ShowFooter="true" FooterStyle-BackColor="Black" FooterStyle-ForeColor="White" OnDataBound="GridView1_DataBound"
                FooterStyle-HorizontalAlign="Right" HeaderStyle-Font-Size="X-Small">
                <Columns>
                    <asp:BoundField DataField="YYYYMMDD" HeaderText="Date" SortExpression="YYYYMMDD" 
                        ItemStyle-Width="30px" ItemStyle-HorizontalAlign="Center" HeaderStyle-BackColor="#FDF8E4">                    
                        <ItemStyle HorizontalAlign="Center" Width="20px"></ItemStyle>
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Service" ItemStyle-Width="50px" ItemStyle-HorizontalAlign="left" 
                         HeaderStyle-BackColor="#FDF8E4">
                        <itemtemplate>
					        <%# GetServiceName(Container.DataItem("SV_ID"))%>
				        </itemtemplate>
                        <ItemStyle HorizontalAlign="Left" Width="50px"></ItemStyle>
                    </asp:TemplateField>
                    <asp:BoundField DataField="SUBS_NUM" HeaderText="New Sub" ItemStyle-Width="30px" 
                        ItemStyle-HorizontalAlign="right" DataFormatString="{0:###,##0}"  HeaderStyle-BackColor="#FDF8E4">
                        <ItemStyle HorizontalAlign="Right" Width="30px"></ItemStyle>
                    </asp:BoundField> 
                    <asp:BoundField DataField="UNSUBS_NUM" HeaderText="Un Sub" ItemStyle-Width="30px" 
                        ItemStyle-HorizontalAlign="right" DataFormatString="{0:###,##0}"  HeaderStyle-BackColor="#FDF8E4">
                        <ItemStyle HorizontalAlign="Right" Width="30px"></ItemStyle>
                    </asp:BoundField> 
                    <asp:BoundField DataField="TOTAL_TRANS" HeaderText="Total Trans" ItemStyle-Width="30px" 
                        ItemStyle-HorizontalAlign="right" DataFormatString="{0:###,##0}"  HeaderStyle-BackColor="#FDF8E4">
                        <ItemStyle HorizontalAlign="Right" Width="30px"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="TOTAL_SENT_TRANS" HeaderText="Total Send Trans" ItemStyle-Width="30px" 
                        ItemStyle-HorizontalAlign="right" DataFormatString="{0:###,##0}"  HeaderStyle-BackColor="#FDF8E4">
                        <ItemStyle HorizontalAlign="Right" Width="30px"></ItemStyle>
                    </asp:BoundField>   
                    <asp:BoundField DataField="TOTAL_AMOUNT" HeaderText="Rev Backoffice" ItemStyle-Width="30px" HeaderStyle-ForeColor="#F1D101"
                        ItemStyle-HorizontalAlign="right" DataFormatString="{0:###,##0}" ItemStyle-ForeColor="#F1D101"  HeaderStyle-BackColor="#FDF8E4">
                        <ItemStyle HorizontalAlign="Right" Width="30px"></ItemStyle>
                    </asp:BoundField>  
                    <asp:BoundField DataField="TOTAL_AMOUNT_SENT_DLVR_000" HeaderText="Rev DR Log" ItemStyle-Width="30px"  HeaderStyle-BackColor="#FDF8E4"
                        ItemStyle-HorizontalAlign="right" DataFormatString="{0:###,##0}">
                        <ItemStyle HorizontalAlign="Right" Width="30px"></ItemStyle>
                    </asp:BoundField> 
                    <asp:BoundField DataField="TOTAL_AMOUNT_SENT_000" HeaderText="Rev ***" HeaderStyle-BackColor="#FFCCEC" ItemStyle-Width="30px" 
                        ItemStyle-HorizontalAlign="right" DataFormatString="{0:###,##0}" ItemStyle-BackColor="#FFCCEC">
                        <ItemStyle HorizontalAlign="Right" Width="30px"></ItemStyle>
                    </asp:BoundField> 
                    <asp:TemplateField HeaderText="000" ItemStyle-Width="50px" ItemStyle-HorizontalAlign="right" HeaderStyle-BackColor="#FDF8E4" ItemStyle-ForeColor="#FB11D4">
                        <HeaderTemplate>
                           <a style="text-align:right" onclick="javascript:alert('Message acknowledged by SMSC')" >000</a>  
                        </HeaderTemplate>
                        <itemtemplate>
					       <%# formatNumber(Container.DataItem("DR_SENT_000"))%>
				        </itemtemplate>
                        <FooterTemplate>
                           <%# GetTotalDR("DR")%>
                        </FooterTemplate>
                    </asp:TemplateField>
<%--                    <asp:BoundField DataField="DR_SENT_000" HeaderText="000" ItemStyle-Width="30px" HeaderStyle-ForeColor="#FB11D4"  HeaderStyle-BackColor="#FDF8E4"
                        ItemStyle-HorizontalAlign="right" DataFormatString="{0:###,##0}" ItemStyle-ForeColor="#FB11D4" >
                        <ItemStyle HorizontalAlign="Right" Width="30px"></ItemStyle>
                    </asp:BoundField> --%>
                    <asp:TemplateField HeaderText="000" ItemStyle-Width="50px" ItemStyle-HorizontalAlign="right" HeaderStyle-BackColor="#FDF8E4">
                        <HeaderTemplate>
                           <a style="text-align:right" onclick="javascript:alert('The recipient has no credits')" >003</a>  
                        </HeaderTemplate>
                        <itemtemplate>
					       <%# formatNumber(Container.DataItem("DR_SENT_003"))%>
				        </itemtemplate>
                    </asp:TemplateField> 
                    <asp:TemplateField HeaderText="000" ItemStyle-Width="50px" ItemStyle-HorizontalAlign="right" HeaderStyle-BackColor="#FDF8E4">
                        <HeaderTemplate>
                           <a style="text-align:right" onclick="javascript:alert('Charged failed')" >004</a>  
                        </HeaderTemplate>
                        <itemtemplate>
					       <%# formatNumber(Container.DataItem("DR_SENT_004"))%>
				        </itemtemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="right" HeaderStyle-BackColor="#FDF8E4">
                        <HeaderTemplate>
                           <a style="text-align:right" onclick="javascript:alert('The service is not associated to given subscriber')" >102</a>  
                        </HeaderTemplate>
                        <itemtemplate>
					       <%# formatNumber(Container.DataItem("DR_SENT_102"))%>
				        </itemtemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="right" HeaderStyle-BackColor="#FDF8E4">
                        <HeaderTemplate>
                           <a style="text-align:right" onclick="javascript:alert('Suspended status of postpaid subscribers')" >410</a>  
                        </HeaderTemplate>
                        <itemtemplate>
					       <%# formatNumber(Container.DataItem("DR_SENT_410"))%>
				        </itemtemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="right" HeaderStyle-BackColor="#FDF8E4">
                        <HeaderTemplate>
                           <a style="text-align:right" onclick="javascript:alert('Inactive status of prepaid subscribers')" >412</a>  
                        </HeaderTemplate>
                        <itemtemplate>
					       <%# formatNumber(Container.DataItem("DR_SENT_412"))%>
				        </itemtemplate>
                    </asp:TemplateField> 
                    <asp:TemplateField ItemStyle-HorizontalAlign="right" HeaderStyle-BackColor="#FDF8E4">
                        <HeaderTemplate>
                           <a style="text-align:right" onclick="javascript:alert('Deactive status of prepaid subscribers')" >413</a>  
                        </HeaderTemplate>
                        <itemtemplate>
					       <%# formatNumber(Container.DataItem("DR_SENT_413"))%>
				        </itemtemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Error อื่นๆ" ItemStyle-Width="50px"
                         HeaderStyle-BackColor="#FDF8E4">
                        <itemtemplate>
                           <a style="text-align:right" onclick="javascript:alert('<%# Eval("DR_SENT_OTHER_REMARK") %>')" ><%# Eval("DR_SENT_OTHER") %></a>  
				        </itemtemplate>
                        <ItemStyle HorizontalAlign="Right" Width="50px"></ItemStyle>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="right" HeaderStyle-BackColor="#AEDBFE" HeaderStyle-ForeColor="#4193FC" 
                        ItemStyle-ForeColor="#0107D2" ItemStyle-BackColor="#D1E5FE">
                        <HeaderTemplate>
                           <a style="text-align:right" onclick="javascript:alert('Successfully sent to phone')" >000</a>  
                        </HeaderTemplate>
                        <itemtemplate>
					       <%# formatNumber(Container.DataItem("DR_SENT_DLVR_000"))%>
				        </itemtemplate>
                        <FooterTemplate>
                            <%# GetTotalDR("")%>
                        </FooterTemplate>
                    </asp:TemplateField> 
                    <asp:TemplateField ItemStyle-HorizontalAlign="right" HeaderStyle-BackColor="#AEDBFE" ItemStyle-BackColor="#D1E5FE">
                        <HeaderTemplate>
                           <a style="text-align:right" onclick="javascript:alert('Delivery to phone failed')" >001</a>  
                        </HeaderTemplate>
                        <itemtemplate>
					       <%# formatNumber(Container.DataItem("DR_SENT_DLVR_001"))%>
				        </itemtemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="right" HeaderStyle-BackColor="#AEDBFE" ItemStyle-BackColor="#D1E5FE">
                        <HeaderTemplate>
                           <a style="text-align:right" onclick="javascript:alert('Memory capacity exceeded')" >200</a>  
                        </HeaderTemplate>
                        <itemtemplate>
					       <%# formatNumber(Container.DataItem("DR_SENT_DLVR_200"))%>
				        </itemtemplate>
                    </asp:TemplateField>   
                    <asp:TemplateField ItemStyle-HorizontalAlign="right" HeaderStyle-BackColor="#AEDBFE" ItemStyle-BackColor="#D1E5FE">
                        <HeaderTemplate>
                           <a style="text-align:right" onclick="javascript:alert('Subscriber error or not support')" >201</a>  
                        </HeaderTemplate>
                        <itemtemplate>
					       <%# formatNumber(Container.DataItem("DR_SENT_DLVR_201"))%>
				        </itemtemplate>
                    </asp:TemplateField>  
                    <asp:TemplateField ItemStyle-HorizontalAlign="right" HeaderStyle-BackColor="#AEDBFE" ItemStyle-BackColor="#D1E5FE">
                        <HeaderTemplate>
                           <a style="text-align:right" onclick="javascript:alert('Absent subscriber or Subscriber unreachable')" >300</a>  
                        </HeaderTemplate>
                        <itemtemplate>
					       <%# formatNumber(Container.DataItem("DR_SENT_DLVR_300"))%>
				        </itemtemplate>
                    </asp:TemplateField> 
                    <asp:TemplateField ItemStyle-HorizontalAlign="right" HeaderStyle-BackColor="#AEDBFE" ItemStyle-BackColor="#D1E5FE">
                        <HeaderTemplate>
                           <a style="text-align:right" onclick="javascript:alert('Unknown subscriber')" >301</a>  
                        </HeaderTemplate>
                        <itemtemplate>
					       <%# formatNumber(Container.DataItem("DR_SENT_DLVR_301"))%>
				        </itemtemplate>
                    </asp:TemplateField> 
                    <asp:TemplateField ItemStyle-HorizontalAlign="right" HeaderStyle-BackColor="#AEDBFE" ItemStyle-BackColor="#D1E5FE">
                        <HeaderTemplate>
                           <a style="text-align:right" onclick="javascript:alert('Subscriber barring')" >302</a>  
                        </HeaderTemplate>
                        <itemtemplate>
					       <%# formatNumber(Container.DataItem("DR_SENT_DLVR_302"))%>
				        </itemtemplate>
                    </asp:TemplateField>  
                    <asp:TemplateField ItemStyle-HorizontalAlign="right" HeaderStyle-BackColor="#AEDBFE" ItemStyle-BackColor="#D1E5FE">
                        <HeaderTemplate>
                           <a style="text-align:right" onclick="javascript:alert('Mobile Switched Off')" >303</a>  
                        </HeaderTemplate>
                        <itemtemplate>
					       <%# formatNumber(Container.DataItem("DR_SENT_DLVR_303"))%>
				        </itemtemplate>
                    </asp:TemplateField>  
                    <asp:TemplateField ItemStyle-HorizontalAlign="right" HeaderStyle-BackColor="#AEDBFE" ItemStyle-BackColor="#D1E5FE">
                        <HeaderTemplate>
                           <a style="text-align:right" onclick="javascript:alert('Undefined Subscriber')" >304</a>  
                        </HeaderTemplate>
                        <itemtemplate>
					       <%# formatNumber(Container.DataItem("DR_SENT_DLVR_304"))%>
				        </itemtemplate>
                    </asp:TemplateField>  
                    <asp:BoundField DataField="DR_SENT_DLVR_OTHER" HeaderText="Error อื่นๆ" ItemStyle-Width="30px"  HeaderStyle-BackColor="#AEDBFE"
                        ItemStyle-HorizontalAlign="right" DataFormatString="{0:###,##0}"  ItemStyle-BackColor="#D1E5FE">
                        <ItemStyle HorizontalAlign="Right" Width="30px"></ItemStyle>
                    </asp:BoundField> 
                    <asp:TemplateField ItemStyle-HorizontalAlign="right" HeaderStyle-BackColor="#AEDBFE">
                        <HeaderTemplate>
                           <a style="text-align:right" onclick="javascript:alert('Diff DR sent 000 - DR all sent_delivered')" >Diff</a>  
                        </HeaderTemplate>
                        <itemtemplate>
					       <%# formatNumber(Container.DataItem("DIFF_DR_SENT_000_WITH_ALL_DLVR"))%>
				        </itemtemplate>
                    </asp:TemplateField> 
<%--                    <asp:BoundField DataField="DIFF_DR_SENT_000_WITH_ALL_DLVR" HeaderText="Diff" ItemStyle-Width="30px"  HeaderStyle-ForeColor="#FC0712"
                        ItemStyle-HorizontalAlign="right" DataFormatString="{0:###,##0}" ItemStyle-ForeColor="#FC0712">
                        <ItemStyle HorizontalAlign="Right" Width="30px"></ItemStyle>
                    </asp:BoundField> --%>                
                </Columns>
            </asp:GridView>
            <br />
            <div class="row">
                <div class="col-md-12" style="text-align:left; font-weight:bold">
                    <asp:Label ID="lblDiffRev" runat="server" Text="" Visible="false" ></asp:Label>
                </div> 
            </div>
            <div class="row">
                <div class="col-md-12" style="text-align:left; font-weight:bold">
                    <asp:Label ID="lbDiffRev2" runat="server" Text="" Visible="false" ></asp:Label>
                </div> 
            </div>
</asp:Content>
