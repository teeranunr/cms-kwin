﻿Public Class log_menu
    Inherits System.Web.UI.Page
    Private intMnuId As Int16
    Private intUserId As Int16
    Private funCenter As New funCenter
    Private strSql As String
    Private strResponse As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        intMnuId = Trim(Request("mnu_id"))
        If (Request.Cookies("USR") IsNot Nothing) Then
            If (Request.Cookies("USR")("USR_ID") IsNot Nothing) Then
                intUserId = Request.Cookies("USR")("USR_ID")
                'MsgBox(strUserLevel)
            Else

            End If
        Else

        End If
        If intUserId > 0 Then
            strResponse = funCenter.LogMenu(intMnuId, intUserId)
        Else

        End If
        'Response.Write(intUserId)
        Response.Redirect(GenReUrl(intMnuId))
    End Sub

    Function GenReUrl(ByVal mnuId As Int16) As String
        Dim objDs As DataSet
        Dim strUrl As String = ""
        strSql = "SELECT MNU_URL FROM BACKOFFICE_MENU" & _
                " WHERE MNU_ID =" & mnuId
        objDs = funCenter.QueryData(strSql, "BACKOFFICE_MENU")
        If objDs.Tables("BACKOFFICE_MENU").Rows.Count > 0 Then
            Return objDs.Tables("BACKOFFICE_MENU").Rows(0).Item("MNU_URL")
        Else
            Return Nothing
        End If
    End Function
End Class